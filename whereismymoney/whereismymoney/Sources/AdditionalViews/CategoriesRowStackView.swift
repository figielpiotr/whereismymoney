//
//  CategoriesRowStackView.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 24.11.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import Foundation

import UIKit

class CategoryRowStackView: UIStackView {

    
    init(){
        super.init(frame: CGRect.zero)
        self.axis  = NSLayoutConstraint.Axis.horizontal
        self.distribution  = UIStackView.Distribution.equalSpacing   
        self.alignment = UIStackView.Alignment.center
        //self.spacing   = 10.0
        self.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundColor = .yellow
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}

