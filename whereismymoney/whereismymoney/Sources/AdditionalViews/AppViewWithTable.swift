//
//  AppViewWithTable.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 25/03/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import UIKit

typealias EmptyClosure = ()->()

protocol ToolbarButtons {
    func getButtons() -> [UIBarButtonItem]
}

protocol AppViewWithTableDelegate: UITableViewDelegate, UITableViewDataSource {
    
}

protocol AppViewWithTableProtocol: UIViewWithSpinner {
    func configureTableCell(cellClass: AnyClass?, cellReuseIdentifier: String)
}

class AppViewWithTable: UIViewWithSpinner, AppViewWithTableProtocol {
    
    // MARK: Closures
    var onBudgetButtonTap: EmptyClosure?
    var onToolbarButtonTap: ((UIBarButtonItem)->())?
    
    weak var _delegate: AppViewWithTableDelegate?
    var delegate: AppViewWithTableDelegate? {
        get {
            return _delegate
        }
        set {
            _delegate = newValue
            table.delegate = _delegate
            table.dataSource = _delegate
        }
    }
    
    let noEntriesLabel = UILabel()
    var toolbar = UIToolbar()
    var table = UITableView()
    var budgetBtn = UIButton(type: .system)
    var loadSpinner: UIActivityIndicatorView!
    
    override init(){
        super.init()
        setup()
    }
     
    func configureToolbarButtons(_ toolbarButtons: ToolbarButtons) {
        let buttons = toolbarButtons.getButtons()
        self.toolbar.setItems(buttons, animated: false)
        for button in buttons {
            button.target = self
            //button.action = #selector(self.delegate?.toolbarButtonPressed(_:))
            button.action = #selector(toolbarButtonTapped(sender:))
        }
    }
    
    func configureTableCell(cellClass: AnyClass?, cellReuseIdentifier: String) {
        self.table.register(cellClass, forCellReuseIdentifier: cellReuseIdentifier)
    }
    
    func setup() {
        backgroundColor = .white
        clipsToBounds = true
        setupNoEntriesLabel()
        setupToolbar()
        setupBudgetButton()
        setupTable()
        disableButtons()
        setNeedsUpdateConstraints()
        showSpinner()
    }
    
    func setupNoEntriesLabel() {
        self.addSubview(noEntriesLabel)
        noEntriesLabel.translatesAutoresizingMaskIntoConstraints = false
        noEntriesLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        noEntriesLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        noEntriesLabel.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        noEntriesLabel.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        noEntriesLabel.backgroundColor = .red
        noEntriesLabel.text = "No entries"
        noEntriesLabel.textColor = UIColor.appText
        noEntriesLabel.textAlignment = .center
        noEntriesLabel.isEnabled = false
        noEntriesLabel.isHidden = true
    }
    
    func setupToolbar() {
        self.addSubview(toolbar)
        toolbar.translatesAutoresizingMaskIntoConstraints = false
        toolbar.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        toolbar.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        
        
        toolbar.contentMode = .top
        
        if self.hasNotch {
            toolbar.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.08).isActive = true
            toolbar.bottomAnchor.constraint(equalTo: self.safeAreaBottomAnchor, constant: 0).isActive = true
        } else {
            toolbar.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.09).isActive = true
            toolbar.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        }
        
        toolbar.isTranslucent = false
        toolbar.backgroundColor = UIColor.appGreen
        toolbar.tintColor = UIColor.appText
        toolbar.barTintColor = UIColor.appGreen
    }
    
    func setupBudgetButton() {
        self.addSubview(budgetBtn)
        budgetBtn.translatesAutoresizingMaskIntoConstraints = false
        budgetBtn.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        budgetBtn.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        budgetBtn.bottomAnchor.constraint(equalTo: toolbar.topAnchor).isActive = true
        budgetBtn.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.07).isActive = true
        budgetBtn.setTitle("Budget: 0 / 0", for: .normal)
        budgetBtn.backgroundColor = .appText
        budgetBtn.setTitleColor(.white, for: .normal)
        budgetBtn.addTarget(self.delegate, action: #selector(budgetBtnTapped), for: .touchUpInside)
    }
    
    func setupTable(){
        self.addSubview(table)
        table.isHidden = true
        table.translatesAutoresizingMaskIntoConstraints = false
        table.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        table.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        table.bottomAnchor.constraint(equalTo: budgetBtn.topAnchor).isActive = true
        table.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        table.separatorInset = .zero
        table.layoutMargins = .zero
        table.allowsMultipleSelectionDuringEditing = true
    }
    
    func showTable() {
        self.table.alpha = 0
        self.table.isHidden = false
        UIView.animate(withDuration: 0.1, animations: {
                 self.table.alpha = 1
            }, completion:  nil)
    }
    
    func hideTable(post: @escaping (()->()) = {}) {
        self.table.alpha = 1
        UIView.animate(withDuration: 0.1, animations: {
            self.table.alpha = 0
        }, completion:  {
           (value: Bool) in
               self.table.isHidden = true
                post()
        })
    }

    func disableButtons() {
        for button in self.toolbar.items ?? [] {
            button.isEnabled = false
        }
        self.budgetBtn.isEnabled = false
    }

    func enableButtons() {
        for button in self.toolbar.items ?? [] {
            button.isEnabled = true
        }
        self.budgetBtn.isEnabled = true
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    @objc func toolbarButtonTapped(sender: UIBarButtonItem) {
        onToolbarButtonTap?(sender)
    }
    
}

// MARK: Actions
extension AppViewWithTable {
    
    @objc func budgetBtnTapped() {
        onBudgetButtonTap?()
    }
    
    
}
