//
//  UIViewWithSpinner.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 16/03/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import UIKit

class UIViewWithSpinner: UIView {

    var spinner: UIActivityIndicatorView!
    
    init(){
        super.init(frame: CGRect.zero)
        self.setupSpinner()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setupSpinner()
    }

    private func setupSpinner() {
        spinner = UIActivityIndicatorView(style: .gray)
        self.addSubview(spinner)
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        spinner.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        spinner.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.01).isActive = true
        spinner.heightAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.01).isActive = true
        spinner.isHidden = true
    }
    
    func showSpinner(pre: (()->()) = {}) {
        if let _ = spinner {
            pre()
            self.spinner.startAnimating()
            self.spinner.alpha = 0
            self.spinner.isHidden = false
            UIView.animate(withDuration: 0.1, animations: {
                     self.spinner.alpha = 1
                }, completion:  nil)
        }
    }
    
    func hideSpinner(post: @escaping(()->()) = {}) {
        if let _ = spinner {
            self.spinner.alpha = 1
            UIView.animate(withDuration: 0.1, animations: {
                self.spinner.alpha = 0
            }, completion:  {
               (value: Bool) in
                    self.spinner.isHidden = true
                    self.spinner.stopAnimating()
                    post()
            })
        }
    }
}
