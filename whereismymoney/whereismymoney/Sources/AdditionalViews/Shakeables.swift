//
//  Shakeables.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 13.11.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import Foundation
import UIKit

class ShakeableUIDatePicker: UIDatePicker, Shakeable {
    
}

class ShakeableUIPickerView: UIPickerView, Shakeable {
    
}

class ShakeableUIButton: UIButton, Shakeable {
    
}

class ShakeableUiTextField: UITextField, Shakeable {
    
}

class ShakeableCurrencyField: CurrencyField, Shakeable {
    
}

class ShakeableUIStackView: UIStackView, Shakeable {
    
}
