//
//  RoundButton.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 28.02.2018.
//  Copyright © 2018 Piotr Figiel. All rights reserved.
//

import Foundation
import UIKit

class RoundButton: UIButton {
    
    override func willMove(toWindow newWindow: UIWindow?) {
        super.willMove(toWindow: newWindow)
        self.setNeedsLayout()
        self.layoutIfNeeded()
        self.layer.cornerRadius = self.frame.width/2;
    }
    
}
