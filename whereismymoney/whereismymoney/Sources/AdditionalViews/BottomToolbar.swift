//
//  Toolbar.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 26/02/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

//import UIKit

//protocol BottomToolbarDelegate where Self: UIViewController {
//    func buttonPressed(button: UIBarButtonItem)
//}
//
//protocol BottomToolbarProtocol where Self: UIToolbar {
//    var vcdelegate: BottomToolbarDelegate? {get set}
//    func setupButtons()
//}
//
//class BottomToolbar: UIToolbar {
//    weak var vcdelegate: BottomToolbarDelegate?
//    static let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
//
//    init(){
//        super.init(frame: CGRect.zero)
//        self.setup()
//    }
//
//    private func setup(){
//        self.translatesAutoresizingMaskIntoConstraints = false
//        self.backgroundColor = UIColor.appGreen
//        self.tintColor = UIColor.appText
//        self.barTintColor = UIColor.appGreen
//    }
//
//    required init?(coder: NSCoder) {
//        super.init(coder: coder)
//        self.setup()
//    }
//
////
////    @objc func buttonPressed(button: UIBarButtonItem) {
////
////    }
//
//}
