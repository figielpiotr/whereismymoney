//
//  RoundCornersUIButton.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 08.08.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import UIKit

let SHADOW_GRAY: CGFloat = 120.0 / 255.0


class RoundCornersUIButton: UIButton {

        
        override func awakeFromNib() {
            super.awakeFromNib()
//            layer.shadowColor = UIColor(red: SHADOW_GRAY, green: SHADOW_GRAY, blue: SHADOW_GRAY, alpha: 0.6).cgColor
//            layer.shadowOpacity = 0.8
//            layer.shadowRadius = 5.0
//            layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
            layer.cornerRadius = 4.0
        }
    
    override func willMove(toWindow newWindow: UIWindow?) {
        super.willMove(toWindow: newWindow)
        layer.cornerRadius = 4.0
    }
    
}
