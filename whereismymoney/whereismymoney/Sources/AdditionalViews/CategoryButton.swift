//
//  CategoryButton.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 24.11.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import Foundation

import UIKit

class CategoryButton: UIButton {
    private var _choosen = false
    var category: Category?
    
    var choosen: Bool {
        set {
            self._choosen = newValue
            if self._choosen {
                self.backgroundColor = UIColor.appGreen
            } else {
                self.backgroundColor = UIColor.appGray
            }
        }
        get {
            return self._choosen
        }
    }    
    
    init(category: Category){
        //super.init(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        super.init(frame: .zero)
        self.category = category
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func configureView(width: CGFloat) {
        //let btnWidth = CGFloat(width/6.8)
        //let btnWidth = CGFloat(width/3)
        //self.frame = CGRect(x: 0, y: 0, width: btnWidth, height: btnWidth)
    
        //let border = CGFloat(10)
    
        
//        self.widthAnchor.constraint(equalToConstant: btnWidth).isActive = true
//        self.heightAnchor.constraint(equalToConstant: btnWidth).isActive = true

        if let c = self.category {
            let image = UIImage(named: "\(c.name)_37.png")
            self.setImage(image, for: UIControl.State.normal)
            self.imageView?.contentMode = .scaleAspectFill
            //self.imageEdgeInsets = UIEdgeInsets(top: border, left: border, bottom: border, right: border)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
