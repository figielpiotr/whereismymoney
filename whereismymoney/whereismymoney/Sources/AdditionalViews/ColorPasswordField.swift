//
//  ColorPasswordField.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 07.04.2018.
//  Copyright © 2018 Piotr Figiel. All rights reserved.
//

import UIKit

class ColorPasswordField: UITextField, Shakeable {

    var minPasswordLength = 6
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        self.textContentType = UITextContentType.password
        self.addTarget(self, action: #selector(self.textFieldDidChange(_:)),
                            for: UIControl.Event.editingChanged)
    }

    @objc func textFieldDidChange(_ textField: UITextField) {
        if (textField.text?.count)! < minPasswordLength {
            self.textColor = UIColor.appRed
        } else {
            self.textColor = UIColor.appGreen
        }
    }
    
    



}
