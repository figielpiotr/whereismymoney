//
//  CategoryStackView.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 24.11.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import Foundation

import UIKit

class CategoryStackView: UIStackView, Shakeable {
    
    var action: ((CategoryButton?)->())?
    
    var delegate: CategoryStackViewDelegate?
    
    var buttons: [CategoryButton] = []
    
    init(){
        super.init(frame: CGRect.zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func configure(categories: [Category], selectedCategory: Category?) {
        for (i, category) in categories.enumerated() {
            let x = UILabel()
            x.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: CGFloat(1/6.8)).isActive = true
            x.heightAnchor.constraint(equalToConstant: CGFloat(self.frame.size.width/6.8)).isActive = true
            x.text = category.name
            self.addArrangedSubview(x)
        }
        
//        var stackView: CategoryRowStackView!
//        for (i, category) in categories.enumerated() {
//            if i % 6 == 0 {
//                stackView = CategoryRowStackView()
//                stackView.translatesAutoresizingMaskIntoConstraints = false
//                stackView.spacing = (self.frame.size.width/6.6)/8
//
//                self.addArrangedSubview(stackView)
//                stackView.heightAnchor.constraint(equalToConstant: 30).isActive = true
//            }
//            let button = CategoryButton(category: category)
//            button.translatesAutoresizingMaskIntoConstraints = false
//            button.addTarget(self, action: #selector(self.performAction(_:)), for: .touchUpInside)
//            if let sc = selectedCategory, sc === category {
//                button.choosen = true
//            }
//            self.buttons.append(button)
//            stackView.addArrangedSubview(button)
//
//            button.configureView(width: self.frame.size.width)
//        }
    }
    
    @objc func performAction(_ sender: CategoryButton?) {
        for button in self.buttons {
            button.choosen = false
        }
        if let button = sender {
            button.choosen = true
        }
        self.delegate?.buttonSelected(sender)
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
