//
//  SeparatorView.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 13/03/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import UIKit

class SeparatorView: UIView {

    init(){
        super.init(frame: CGRect.zero)
        self.setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
  
    private func setup() {
        self.backgroundColor = .appGreen
        self.translatesAutoresizingMaskIntoConstraints = false
        self.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        if let superview = self.superview {
            self.widthAnchor.constraint(equalTo: superview.widthAnchor, multiplier: 1).isActive = true
        }
    }
}
