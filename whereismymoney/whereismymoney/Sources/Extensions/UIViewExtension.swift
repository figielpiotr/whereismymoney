//
//  UIViewExtension.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 06/05/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import Foundation
import UIKit


extension UIView {
    public var hasNotch: Bool {
        let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        return bottom > 0
    }
}
