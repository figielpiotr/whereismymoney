//
//  ViewControllerExtension.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 17.03.2018.
//  Copyright © 2018 Piotr Figiel. All rights reserved.
//

import UIKit

extension UIViewController {
    func showView(_ view: UIView){
        if view.isHidden {
            view.isHidden = false
            UIView.animate(withDuration: 0.1, delay: 0.2, options: .curveEaseInOut,
                           animations: {view.alpha = 1},
                           completion: { _ in
            })
        }
    }
    
    func hideView(_ view: UIView, completion: @escaping () -> Void) {
        if !view.isHidden {
            UIView.animate(withDuration: 0.1, delay: 0.2, options: .curveEaseInOut,
                           animations: {view.alpha = 0},
                           completion: { _ in
                            view.isHidden = true
                            completion()
            })
        } else {
            completion()
        }
    }
    
    func showAlert(_ title: String, _ message: String, _ actions: [UIAlertAction]) {
        let titleFont = [NSAttributedString.Key.font: UIFont(name: "Helvetica Neue", size: 15.0)!]
        let messageFont = [NSAttributedString.Key.font: UIFont(name: "Helvetica Neue", size: 11.0)!]
        let titleAttrString = NSMutableAttributedString(string: title, attributes: titleFont)
        let messageAttrString = NSMutableAttributedString(string: message, attributes: messageFont)
        //let defaultAction = UIAlertAction(title: "OK", style: .default)
        let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
        alert.setValue(titleAttrString, forKey: "attributedTitle")
        alert.setValue(messageAttrString, forKey: "attributedMessage")
        for action in actions {
            alert.addAction(action)
        }
        
        alert.view.tintColor = UIColor.appGreen
        alert.view.layer.cornerRadius = 40
        self.present(alert, animated: true) {}
    }
}
