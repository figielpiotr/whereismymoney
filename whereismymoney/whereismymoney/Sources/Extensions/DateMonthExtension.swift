//: Playground - noun: a place where people can play

import UIKit

//
//  NSDateMonthExtension.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 05.04.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import Foundation
import UIKit

extension Date {
    
    func day() -> Int {
        return Calendar.current.component(Calendar.Component.day, from: self)
    }
    
    func month() -> Int {
        return Calendar.current.component(Calendar.Component.month, from: self)
    }
    
    func year() -> Int {
        return Calendar.current.component(Calendar.Component.year, from: self)
    }
    
    
    func startOfMonth() -> Date? {
        let calendar = Calendar.current
        guard let interval = calendar.dateInterval(of: .month, for: self) else { return nil }
        return interval.start
    }
    
    func dateByAddingMonths(monthsToAdd: Int) -> Date? {
        
        let calendar = Calendar.current
        var months = DateComponents()
        months.month = monthsToAdd
        return calendar.date(byAdding: months, to: self)
    }
    
    func endOfMonth() -> Date? {
        guard let startOfMonth = self.startOfMonth() else { return nil }
        guard let endOfMonth = Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: startOfMonth) else {return nil}
        return endOfMonth
    }
    
    func startOfQuarter() -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let month = self.month()
        let year = self.year()
        if(month > 3 && month < 7) {
            return dateFormatter.date(from: "\(year)-04-01")
        } else if(month > 6 && month < 10) {
            return dateFormatter.date(from: "\(year)-07-01")
        } else if(month > 9 && month < 13) {
            return dateFormatter.date(from: "\(year)-10-01")
        }
        return dateFormatter.date(from: "\(year)-01-01")
    }
    
    func endOfQuarter() -> Date? {
        return self.startOfQuarter()?.dateByAddingMonths(monthsToAdd: 2)?.endOfMonth()
    }
    
    func startOfYear() -> Date? {
        let calendar = Calendar.current
        guard let interval = calendar.dateInterval(of: .year, for: Date()) else { return nil }
        return interval.start
    }
    
    func endOfYear() -> Date? {
        return self.startOfYear()?.dateByAddingMonths(monthsToAdd: 11)?.endOfMonth()
    }

    func convertToLocalTime() -> Date {
        let timeZone = Calendar.current.timeZone
        let targetOffset = TimeInterval(timeZone.secondsFromGMT(for: self))
        let localOffeset = TimeInterval(TimeZone.autoupdatingCurrent.secondsFromGMT(for: self))
        return self.addingTimeInterval(targetOffset + localOffeset)
    }
    
//    func convertToLocalTime(fromTimeZone timeZoneAbbreviation: String) -> Date? {
//        if let timeZone = TimeZone(abbreviation: timeZoneAbbreviation) {
//            let targetOffset = TimeInterval(timeZone.secondsFromGMT(for: self))
//            let localOffeset = TimeInterval(TimeZone.autoupdatingCurrent.secondsFromGMT(for: self))
//
//            return self.addingTimeInterval(targetOffset + localOffeset)
//        }
//
//        return nil
//    }
    
    func getMonthName() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM"
        let strMonth = dateFormatter.string(from: self)
        return strMonth
    }
    
    static func getQuarterFrom(month: Int) -> Int {
        var quarter = 1
        if(month > 3 && month < 7) {
            quarter = 2
        } else if(month > 6 && month < 10) {
            quarter = 3
        } else if(month > 9 && month < 13) {
            quarter = 4
        }
        return quarter
    }
}

