//
//  StringExtension.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 22.08.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import Foundation

extension String {
    var doubleValue: Double {
        let nf = NumberFormatter()
        nf.decimalSeparator = "."
        if let result = nf.number(from: self) {
            return result.doubleValue
        } else {
            nf.decimalSeparator = ","
            if let result = nf.number(from: self) {
                return result.doubleValue
            }
        }
        return 0
    }
}
