//
//  UIColorExtension.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 13.11.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    static var appText: UIColor {
        return UIColor(red: 26/255, green: 33/255, blue: 45/255, alpha: 1)
    }
    
    static var appGreen: UIColor {
        return UIColor(red: 131/255, green: 150/255, blue: 14/255, alpha: 1)
    }
    
    static var appGray: UIColor {
        return UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1)
    }
    
    static var appLightGreen: UIColor {
        return UIColor(red: 251/255, green: 252/255, blue: 240/255, alpha: 1)
    }
    
    static var appRed: UIColor {
        return UIColor(red: 191/255, green: 49/255, blue: 0/255, alpha: 1)
    }
    
    static var appYellow: UIColor {
        return UIColor(red: 251/255, green: 252/255, blue: 240/255, alpha: 1)
    }
    
    private static var graphColors = ["BF3100", "514B23", "330F0A", "394F49", "EFDD8D", "5BC0EB", "FDE74C", "211A1E", "01295F", "83960E", "94B9AF", "90A583", "942911", "593837", "F5BB00", "EC9F05", "D76A03", "437F97", "5D3A00"]
    private static var appGraphColors:[UIColor] = []
    
    static func getGraphColors() -> [UIColor] {
        if appGraphColors.count == 0 {
            for hexColor in graphColors {
                appGraphColors.append(hexStringToUIColor(hex: hexColor))
            }
        }
        return appGraphColors
    }
    
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
