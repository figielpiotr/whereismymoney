//
//  LoginView.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 26/02/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import FirebaseAuth
import TwitterKit
import GoogleSignIn

class AppLoginView: UIView {
    private var didSetupConstraints = false
    
    weak var _delegate: LoginVC?
    
    var delegate: LoginVC? {
        get {
            if let _ = _delegate {
                return _delegate
            }
            return nil
        }
        set {
            _delegate = newValue
            setButtonDelegates()
        }
    }
    
    let wrapper = UIStackView()
    
    var gpButton = UIButton()
    
    var fbButton = UIButton()
    
    var twButton = UIButton()
    
    var emailInput = ShakeableUiTextField()
    
    var passwordInput = ColorPasswordField()
    
    var loginButton = RoundCornersUIButton()

    
    
    var logo = UIImageView()

    
    init(){
        super.init(frame: CGRect.zero)
        self.setup()
    }
 
    func setButtonDelegates() {
        if let delegate = self.delegate {
            fbButton.addTarget(delegate, action: #selector(delegate.facebookLoginPressed), for: .touchUpInside)
            twButton.addTarget(delegate, action: #selector(delegate.twitterLoginPressed), for: .touchUpInside)
            gpButton.addTarget(delegate, action: #selector(delegate.googleLoginPressed), for: .touchUpInside)
        }
    }
    
    private func setup() {
        self.backgroundColor = .white
        clipsToBounds = true
        setupBackgroundImage()
        setupStackView()
        setupLogo()
        setupEmailLogin()
        setupSocialMediaLogin()
        self.setNeedsUpdateConstraints()
    }

    private func setupStackView() {
        self.addSubview(wrapper)
        wrapper.translatesAutoresizingMaskIntoConstraints = false
        wrapper.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.9).isActive = true
        wrapper.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.9).isActive = true
        wrapper.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        wrapper.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        wrapper.axis = .vertical
        wrapper.contentMode = .scaleAspectFit
        wrapper.distribution = .equalSpacing
        wrapper.spacing = 7
        wrapper.alignment = .center
        
    }
    
    private func setupBackgroundImage() {
        let background = UIImage(named: "bg")
        var imageView : UIImageView!
        imageView = UIImageView(frame: self.bounds)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = self.center
        self.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        imageView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        imageView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        imageView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        self.sendSubviewToBack(imageView)
        self.backgroundColor = UIColor.appGreen
    }
    
    private func setupLogo(){
        wrapper.addArrangedSubview(logo)
        logo.translatesAutoresizingMaskIntoConstraints = false
        let image = UIImage(named: "pig_logo_green.png")
        logo.image = image
        logo.widthAnchor.constraint(equalTo: wrapper.widthAnchor, multiplier: 0.7).isActive = true
        logo.heightAnchor.constraint(equalTo: wrapper.widthAnchor, multiplier: 0.7).isActive = true
    }
    
    private func setupEmailLogin() {
        let emailWrapper = UIStackView()
        wrapper.addArrangedSubview(emailWrapper)
        emailWrapper.translatesAutoresizingMaskIntoConstraints = false
        emailWrapper.widthAnchor.constraint(equalTo: wrapper.widthAnchor, multiplier: 1).isActive = true
        emailWrapper.heightAnchor.constraint(equalTo: wrapper.heightAnchor, multiplier: 0.23).isActive = true
        emailWrapper.axis = .vertical
        emailWrapper.contentMode = .scaleAspectFit
        emailWrapper.distribution = .equalSpacing
        //emailWrapper.spacing = 0
        emailWrapper.alignment = .center
        
        
        
        let inputs: [(UITextField, String)] = [(emailInput, "Login"), (passwordInput, "Password")]
        for input in inputs {
            let textField = input.0
            let name = input.1
            emailWrapper.addArrangedSubview(textField)
            textField.translatesAutoresizingMaskIntoConstraints = false
            textField.widthAnchor.constraint(equalTo: emailWrapper.widthAnchor).isActive = true
            textField.heightAnchor.constraint(equalTo: emailWrapper.heightAnchor, multiplier: 0.27).isActive = true
            textField.placeholder = name
            textField.font = UIFont.systemFont(ofSize: 15)
            textField.borderStyle = UITextField.BorderStyle.roundedRect
            textField.autocorrectionType = UITextAutocorrectionType.no
            textField.autocapitalizationType = .none
            if name == "Login" {
                textField.keyboardType = UIKeyboardType.emailAddress
            } else {
                textField.keyboardType = UIKeyboardType.default
            }
            
            textField.returnKeyType = UIReturnKeyType.done
            textField.clearButtonMode = UITextField.ViewMode.whileEditing
            textField.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
            textField.textAlignment = .center
            if name == "Password" {
                textField.textContentType = .password
                textField.isSecureTextEntry = true
            }
        }

        emailWrapper.addArrangedSubview(loginButton)
        
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        loginButton.widthAnchor.constraint(equalTo: emailWrapper.widthAnchor).isActive = true
        loginButton.heightAnchor.constraint(equalTo: emailWrapper.heightAnchor, multiplier: 0.27).isActive = true
        loginButton.setTitle("Login", for: .normal)
        loginButton.backgroundColor = UIColor.appText
        loginButton.setTitleColor(.white, for: .normal)
        loginButton.addTarget(self, action: #selector(loginButtonPressed), for: .touchUpInside)
    }
    
    private func setupSocialMediaLogin() {
        let or = UILabel()
        or.text = "OR"
        or.textColor = .white
        or.textAlignment = .center
        or.font = or.font.withSize(12)

        wrapper.addArrangedSubview(or)
        or.widthAnchor.constraint(equalTo: wrapper.widthAnchor).isActive = true
        or.heightAnchor.constraint(equalTo: wrapper.heightAnchor, multiplier: 0.07).isActive = true
        
        let socialButtons = UIStackView()
        socialButtons.translatesAutoresizingMaskIntoConstraints = false
        wrapper.addArrangedSubview(socialButtons)
        socialButtons.widthAnchor.constraint(equalTo: wrapper.widthAnchor, multiplier: 0.8).isActive = true
        socialButtons.heightAnchor.constraint(equalTo: wrapper.heightAnchor, multiplier: 0.1).isActive = true
        socialButtons.centerXAnchor.constraint(equalTo: wrapper.centerXAnchor).isActive = true
        socialButtons.axis = .horizontal
        socialButtons.contentMode = .scaleAspectFit
        socialButtons.distribution = .equalSpacing
        socialButtons.alignment = .center
        
        
        let buttons: [(UIButton, String)] = [(fbButton,"Facebook"), (twButton,"Twitter"), (gpButton,"Google")]
        
        for input in buttons {
            let button = input.0
            let name = input.1
            socialButtons.addArrangedSubview(button)
            button.translatesAutoresizingMaskIntoConstraints = false
            button.widthAnchor.constraint(equalTo: socialButtons.heightAnchor, multiplier: 1).isActive = true
            button.heightAnchor.constraint(equalTo: socialButtons.heightAnchor, multiplier: 1).isActive = true
            let image = UIImage(named: name.lowercased()+"_circle")
            button.setImage(image, for: .normal)
        }
        
        let loginDescription = UILabel()
        loginDescription.translatesAutoresizingMaskIntoConstraints = false
        loginDescription.numberOfLines = 4
        loginDescription.text = "Use the option above to login or register. \n If you don't have an account, it will be created for you."
        loginDescription.textAlignment = .center
        loginDescription.font = loginDescription.font.withSize(10)
        loginDescription.textColor = .white
        wrapper.addArrangedSubview(loginDescription)
        loginDescription.widthAnchor.constraint(equalTo: wrapper.widthAnchor).isActive = true
    }
    
    @objc private func loginButtonPressed() {
        if let delegate = delegate {
            delegate.loginPressed(email: emailInput.text ?? "", password: passwordInput.text ?? "")
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
}
