//
//  LoginViewController.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 14.07.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import UIKit

import FBSDKCoreKit
import FBSDKLoginKit
import FirebaseAuth
import TwitterKit
import GoogleSignIn



let KEY_UID = "uid"


//class LoginVC: UIViewController, GIDSignInUIDelegate, GIDSignInDelegate, HasCustomView {
class LoginVC: UIViewController, GIDSignInDelegate, HasCustomView {

    typealias CustomView = AppLoginView

    override func loadView() {
        view = CustomView()
        self.edgesForExtendedLayout = []
    }
    
    weak var mainVC: MainVC?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customView.delegate = self
        navigationController?.setNavigationBarHidden(true, animated: true)
        self.hideKeyboardWhenTappedAround()
//        GIDSignIn.sharedInstance().uiDelegate = self
//        GIDSignIn.sharedInstance().delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func showAlert(title: String, message: String?){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func loginPressed(email: String, password: String) {
        if (email.count > 0 && password.count > 0) {
            AuthService.instance.login(email: email, password: password, onComplete: { (errorMsg, user) in
                if(errorMsg != nil) {
                    self.showAlert(title: "Unable to login", message: errorMsg)
                } else {
                    if let u = user, !u.isEmailVerified, let e = u.email ?? "" {
                        let alert = UIAlertController(title: "Email not verified", message: "Your email adress was not verified! Please follow the link sent to you to \(e) and try to login again. If you didn't get the verification email, use \"Resend\" button.", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                        alert.addAction(UIAlertAction(title: "Resend", style: .default, handler: { (action) in
                            AuthService.instance.sendVerificationEmail(user: user as! User)
                        }))
                        self.present(alert, animated: true, completion: nil)
                        AuthService.instance.logout(onComplete: { (error, user) in
                            
                        })
                    } else {
                        self.reloadMainVCData(user: user)
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                }  
            })
        } else {
            if email.count == 0 {
                self.customView.emailInput.shake()
            }
            if password.count == 0 {
                self.customView.passwordInput.shake()
            }
        }
    }
    
    @objc func facebookLoginPressed() {
        let loginManager = LoginManager()

        loginManager.logIn(permissions: [], from: self) { [weak self] (result, error) in
            guard error == nil else {
                self?.showAlert(title: "Unable to login", message: error!.localizedDescription)
                return
            }
            
            guard let result = result, !result.isCancelled else {
                return
            }
          
            AuthService.instance.loginFacebook(result: result) { (error, user) in
                if(error != nil) {
                    self?.showAlert(title: "Unable to login", message: error!)
                } else {
                    self?.reloadMainVCData(user: user)
                    self?.navigationController?.popToRootViewController(animated: true)
                }
            }
        }

    }
    
    @objc func twitterLoginPressed() {
        TWTRTwitter.sharedInstance().logIn { (session, error) in
            AuthService.instance.loginTwitter(session: session, error: error) { (error, user) in
                if(error != nil) {
                    self.showAlert(title: "Unable to login", message: error!)
                } else {
                    self.reloadMainVCData(user: user)
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
        }
    }
    

    @objc func googleLoginPressed() {
        GIDSignIn.sharedInstance().signIn()
    }
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if let e = error {
            if (e as NSError).code != GIDSignInErrorCode.canceled.rawValue {
                self.showAlert(title: "Unable to login", message: "\(e)")
            }
            return
        }
        let authentication = user.authentication
        let credential = GoogleAuthProvider.credential(withIDToken: (authentication?.idToken)!, accessToken: (authentication?.accessToken)!)
        
        AuthService.instance.loginGoogle(credential: credential) { (error, user) in
            if(error != nil) {
                self.showAlert(title: "Unable to login", message: error)
            } else {
                self.reloadMainVCData(user: user)
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        if let e = error {
            self.showAlert(title: "Problem with sign out", message: "\(e)")
        }
        try! Auth.auth().signOut()
    }
    
    func reloadMainVCData(user: Any?){
        
        if let u = user as? User {
            DataService.shared.setBudgetForUser(user: u, completionHandler: {
                if let mvc = self.mainVC {
                    //mvc.loadData()
                }
            })
        }
    }

}
