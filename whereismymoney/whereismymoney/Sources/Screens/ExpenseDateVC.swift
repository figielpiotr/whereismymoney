//
//  ExpenseDateVC.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 14.12.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import UIKit
import GoogleMobileAds



class ExpenseDateVC: AdvertableVC, AppNavigationDelegate {

    weak var expenseDetailsVC: ExpenseDetailsVC?
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBOutlet weak var gadBanner: GADNativeExpressAdView!
    
    
    override func viewDidLoad() {
        self.bannerView = gadBanner
        super.viewDidLoad()
        showSaveBtn()
        if let edvc = expenseDetailsVC {
            //datePicker.date = edvc.expenseDate
        }
        gadBanner.adUnitID = GAD_UNIT_ID
        gadBanner.rootViewController = self
        let request = GADRequest()
        gadBanner.load(request)
    }

    func save() {
        if let edvc = expenseDetailsVC {
//            edvc.expenseDate = datePicker.date
        }
        navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func hideKeyboard() {
        
    }
}
