//
//  AdvertableVC.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 01.03.2018.
//  Copyright © 2018 Piotr Figiel. All rights reserved.
//

import UIKit
import GoogleMobileAds

class AdvertableVC: AppNavigationVC, Advertable {
    var bannerView: GADNativeExpressAdView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        prepareAds()
    }
}
