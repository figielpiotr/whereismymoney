//
//  IncomeDetailsVCViewController.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 19.06.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import UIKit
import Firebase
import GoogleMobileAds


class IncomeDetailsVC: AdvertableVC, AppNavigationDelegate, UITextFieldDelegate, HasCustomView {
    typealias CustomView = IncomeDetailsView

    override func loadView() {
        view = CustomView()
        self.edgesForExtendedLayout = []
    }
    
    var income: Income!
    weak var budgetPeriod: BudgetPeriod!
    weak var incomeListVC: IncomeListVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.definesPresentationContext = true
        self.customView.delegate = self
        
        if income != nil {
            self.customView.incomeNameInput.text = income.name
            self.customView.incomeValueInput.text = NumberFormatter.localizedString(from: NSNumber(value: income.value), number: NumberFormatter.Style.currency)
        }
    }
    
    func hideKeyboard(){
        self.customView.incomeNameInput.resignFirstResponder()
        self.customView.incomeValueInput.resignFirstResponder()
    }
    
    @objc func buttonPressed(_ button: UIButton) {
        if button.tag == IncomeDetailsButtonsEnum.ADD.rawValue {
            self.save()
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func save(){
        //TODO: usunąć force unwrap
        let incomeNameInput = customView.incomeNameInput
        let incomeValueInput = customView.incomeValueInput
        
        let value = incomeValueInput.doubleValue
        guard let name = incomeNameInput.text, !name.isEmpty else {
            incomeNameInput.shake()
            if value == 0.0 {
                incomeValueInput.shake()
            }
            return
        }
        
        guard value != 0.0 else {
            incomeValueInput.shake()
            return
        }
        
        let now = Date()
        let ds = DataService.ds
        
        if self.income != nil {
            self.income.name = name
            self.income.value = value
            ds.updateIncome(income: income)
        } else {
            let factory = IncomeFactory.incomeFactory
            let income = factory.createIncome(id: "", name: name, value: value, createdAt: now)
            self.budgetPeriod.addIncome(income: income)
            //self.budgetPeriod.sortIncomes(order: SettingsService.instance.incomeSort)
            ds.addNewIncome(income: income)
        }
        self.incomeListVC.updateTable()
        self.dismiss(animated: true, completion: nil)
    }
}
