//
//  IncomeListView.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 25/03/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import UIKit

class IncomeListView: AppViewWithTable {
    
    override init(){
        super.init()
    }
    
    override func setupBudgetButton(){
        super.setupBudgetButton()
        self.budgetBtn.isUserInteractionEnabled = false
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}
