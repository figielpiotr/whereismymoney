//
//  IncomeDetailsView.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 01/04/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import UIKit

enum IncomeDetailsButtonsEnum: Int, CaseIterable {
    case ADD = 0
    case CANCEL = 1
}

class IncomeDetailsView: UIView {
    private var didSetupConstraints = false
    
    weak var delegate: IncomeDetailsVC?
    
    var incomeNameInput = ShakeableUiTextField()
    var incomeValueInput = ShakeableCurrencyField()
    
//    var saveButton = RoundCornersUIButton()
//    var cancelButton = RoundCornersUIButton()
    
    let wrapper = UIStackView()
    
    init(){
        super.init(frame: CGRect.zero)
        self.setup()
    }
    
    private func setup() {
        self.backgroundColor = UIColor(white: 0, alpha: 0.5)
        clipsToBounds = true

        setupWrapper()
        setupTextFields()
        setupButtons()

        self.setNeedsUpdateConstraints()
    }
    
    private func setupWrapper() {
        let wrapperView = UIView()
        self.addSubview(wrapperView)
        wrapperView.translatesAutoresizingMaskIntoConstraints = false
        wrapperView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.8).isActive = true
        wrapperView.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.3).isActive = true
        wrapperView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        wrapperView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        wrapperView.backgroundColor = .white
        wrapperView.layer.borderColor = UIColor.appText.cgColor
        wrapperView.layer.borderWidth = 1
        wrapperView.layer.cornerRadius = 4.0
        
        wrapperView.addSubview(wrapper)
        wrapper.translatesAutoresizingMaskIntoConstraints = false
        wrapper.widthAnchor.constraint(equalTo: wrapperView.widthAnchor, multiplier: 0.9).isActive = true
        wrapper.heightAnchor.constraint(equalTo: wrapperView.heightAnchor, multiplier: 0.8).isActive = true
        wrapper.centerXAnchor.constraint(equalTo: wrapperView.centerXAnchor).isActive = true
        wrapper.centerYAnchor.constraint(equalTo: wrapperView.centerYAnchor).isActive = true
        wrapper.axis = .vertical
        wrapper.contentMode = .scaleAspectFit
        wrapper.distribution = .equalSpacing
        wrapper.spacing = 0
        wrapper.alignment = .center
    }
    
    private func setupTextFields() {
        incomeNameInput.delegate = self.delegate
        incomeValueInput.delegate = NumbersUITextFieldDelegate.numbersUITextFieldDelegate
        incomeNameInput.placeholder = "Name"
        incomeValueInput.placeholder = "Value"
        for textField in [incomeNameInput, incomeValueInput] {
            wrapper.addArrangedSubview(textField)
            textField.translatesAutoresizingMaskIntoConstraints = false
            textField.widthAnchor.constraint(equalTo: wrapper.widthAnchor).isActive = true
            textField.heightAnchor.constraint(equalTo: wrapper.heightAnchor, multiplier: 0.3).isActive = true
            textField.font = UIFont.systemFont(ofSize: 15)
            textField.borderStyle = UITextField.BorderStyle.roundedRect
            textField.autocorrectionType = UITextAutocorrectionType.no
            textField.keyboardType = UIKeyboardType.default
            textField.returnKeyType = UIReturnKeyType.done
            textField.clearButtonMode = UITextField.ViewMode.whileEditing
            textField.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        }
    }
    
    private func setupButtons() {
        let buttons = UIStackView()
        wrapper.addArrangedSubview(buttons)
        buttons.translatesAutoresizingMaskIntoConstraints = false
        buttons.heightAnchor.constraint(equalTo: wrapper.heightAnchor, multiplier: 0.2).isActive = true
        buttons.leadingAnchor.constraint(equalTo: wrapper.leadingAnchor).isActive = true
        buttons.trailingAnchor.constraint(equalTo: wrapper.trailingAnchor).isActive = true
        buttons.axis = .horizontal
        buttons.contentMode = .scaleAspectFit
        buttons.distribution = .fill
        buttons.spacing = 0
        buttons.alignment = .center
        for b in IncomeDetailsButtonsEnum.allCases {
            let button = RoundCornersUIButton()
            buttons.addArrangedSubview(button)
            button.tag = b.rawValue
            button.translatesAutoresizingMaskIntoConstraints = false
            button.widthAnchor.constraint(equalTo: buttons.widthAnchor, multiplier: 0.4).isActive = true
            button.heightAnchor.constraint(equalTo: buttons.heightAnchor, multiplier: 1).isActive = true
            
            button.addTarget(self.delegate, action: #selector(self.delegate?.buttonPressed(_:)), for: .touchUpInside)
            switch b {
            case IncomeDetailsButtonsEnum.ADD:
                button.setTitle("Save", for: .normal)
                button.backgroundColor = .appText
                button.setTitleColor(.white, for: .normal)
            case IncomeDetailsButtonsEnum.CANCEL:
                button.setTitle("Cancel", for: .normal)
                button.backgroundColor = .appGray
                button.setTitleColor(.appText, for: .normal)
            }
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
}
