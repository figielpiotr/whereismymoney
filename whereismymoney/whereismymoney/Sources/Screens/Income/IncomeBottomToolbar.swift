//
//  IncomeBottomToolbar.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 25/03/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import UIKit

enum IncomeBottomTooblarButtonsEnum: Int, CaseIterable {
    case ADD = 0
    case DELETE = 1
}

struct IncomeBottomToolbarButtons: ToolbarButtons {
    let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
    
    func getButtons() -> [UIBarButtonItem] {
        var buttons = [UIBarButtonItem]()
        buttons.append(spacer)
        for b in IncomeBottomTooblarButtonsEnum.allCases {
            let button = UIBarButtonItem(image: UIImage(named: "ico_"+String(describing: b).lowercased()), style: .plain, target: nil, action: nil)
            button.tag = b.rawValue
            buttons.append(button)
            if b != IncomeBottomTooblarButtonsEnum.allCases.last {
                buttons.append(spacer)
            }
        }
        return buttons
    }
}

//struct IncomeBottomToolbarButtons: ToolbarButtons {
//    let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
//    func getButtons() -> [UIBarButtonItem] {
//        var buttons = [UIBarButtonItem]()
//        let addButton = UIBarButtonItem(image: UIImage(named: "ico_add"), style: .plain, target: nil, action: nil)
//        buttons.append(spacer)
//        buttons.append(addButton)
//        buttons.append(spacer)
//        return buttons
//    }
//}
