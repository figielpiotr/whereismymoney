//
//  IncomeVC.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 19.06.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import UIKit

class IncomeListVC: UIViewController, AppViewWithTableDelegate, HasCustomView {
    typealias CustomView = IncomeListView
    
    override func loadView() {
        view = CustomView()
        self.edgesForExtendedLayout = []
    }
    
    var budgetPeriod: BudgetPeriod!
    weak var mainVC: MainVC!
    
    override func viewDidLoad() {
        self.customView.delegate = self
        self.customView.configureToolbarButtons(IncomeBottomToolbarButtons())
        self.customView.configureTableCell(cellClass: IncomeTableViewCell.self, cellReuseIdentifier: "IncomeCell")
//        if let bp = budgetPeriod {
//            bp.sortIncomes(order: SettingsService.instance.incomeSort)
//        }
        self.updateBudgetLabel()
        self.customView.table.isHidden = false
    }

    func toolbarButtonPressed(_ button: UIBarButtonItem) {
        switch button.tag {
        case IncomeBottomTooblarButtonsEnum.ADD.rawValue:
            showIncomeDetails(income: nil)
        case IncomeBottomTooblarButtonsEnum.DELETE.rawValue:
            if !customView.table.isEditing {
                customView.table.setEditing(true, animated: true)
            } else {
                customView.table.beginUpdates()
                removeSelectedIncome()
                customView.table.setEditing(false, animated: true)
                customView.table.reloadData()
                customView.table.endUpdates()
            }
        default:
            return
        }
        
    }
    
    func showIncomeDetails(income: Income?) {
        let vc = IncomeDetailsVC()
        vc.budgetPeriod = self.budgetPeriod
        vc.incomeListVC = self
        if let _ = income {
            vc.income = income
        }
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .coverVertical
        present(vc, animated: true, completion: nil)
    }
    
    func budgetButtonPressed() {
        return
    }
    
    func updateBudgetLabel(){
        let income = budgetPeriod.calculateIncome()
        let incomeFormated = NumberFormatter.localizedString(from: NSNumber.init(value: income.roundTo(places: 2)), number: NumberFormatter.Style.currency)
        self.customView.budgetBtn.setTitle("Budget: \(incomeFormated)", for: .normal)
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let bp = self.budgetPeriod {
            return bp.incomes.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IncomeCell", for: indexPath) as! IncomeTableViewCell
        if let bp = self.budgetPeriod {
            let income = bp.incomes[indexPath.row]
            cell.configureCell(income: income, indexPath.row)
            
        }
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !customView.table.isEditing {
            guard let bp = self.budgetPeriod else { return }
            let incomes = bp.incomes
            
            if incomes.count > 0 {
                let item = incomes[indexPath.row]
                self.showIncomeDetails(income: item)
            }
        }
    }

    func updateTable(){
        self.customView.table.reloadData()
        self.updateBudgetLabel()
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    //refactor
    func removeSelectedIncome() {
        for selected in customView.table.indexPathsForSelectedRows ?? [] {
            removeIncome(indexPath: selected)
        }
    }
    
    func removeIncome(indexPath: IndexPath) {
        if let bp = self.budgetPeriod {
            let income = bp.incomes[indexPath.row]
            bp.removeIncome(income: income)
            DataService.ds.removeIncome(income: income)
            customView.table.deleteRows(at: [indexPath], with: .fade)
        }
    }
}
