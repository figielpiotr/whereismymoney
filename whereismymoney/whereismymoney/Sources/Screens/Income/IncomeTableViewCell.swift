//
//  IncomeTableViewCell.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 19.06.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import UIKit

class IncomeTableViewCell: UITableViewCell {

    var container = UIStackView()
    var incomeLabel = UILabel()
    var incomeValue = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    private func setup() {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0)
        self.selectedBackgroundView = view
        self.tintColor = .appGreen

        
        setupStackView()
        setupLabels()
        self.setNeedsUpdateConstraints()
    }
    
    private func setupStackView() {
        container.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(container)
        container.widthAnchor.constraint(equalTo: self.contentView.widthAnchor, multiplier: 0.9).isActive = true
        container.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor).isActive = true
        container.topAnchor.constraint(equalTo: self.contentView.topAnchor).isActive = true
        container.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor).isActive = true
        container.spacing = 8
        container.distribution = .fill
        container.alignment = .center
        container.axis = .horizontal
        container.contentMode = .scaleToFill
    }
    
    private func setupLabels() {
        for label in [(incomeLabel, NSTextAlignment.left), (incomeValue, NSTextAlignment.left)] {
            label.0.translatesAutoresizingMaskIntoConstraints = false
            container.addArrangedSubview(label.0)
            label.0.heightAnchor.constraint(equalTo: container.heightAnchor, multiplier: 0.8).isActive = true
            label.0.setContentHuggingPriority(UILayoutPriority(rawValue: 249), for: .horizontal)
            label.0.setContentHuggingPriority(UILayoutPriority(rawValue: 249), for: .vertical)
            label.0.textAlignment = label.1
        }
    }
    
    func configureCell(income: Income, _ row: Int)
    {
        self.incomeLabel.text = income.name
        self.incomeValue.text = NumberFormatter.localizedString(from: NSNumber.init(value: income.value), number: NumberFormatter.Style.currency)
        if row % 2 == 1 {
            self.backgroundColor = UIColor(red: 251/255, green: 252/255, blue: 240/255, alpha: 1)
        } else {
            self.backgroundColor = .white
        }
    }
    
}
