//
//  MainView.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 26/02/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import UIKit

class MainView: AppViewWithTable {
    
    override init(){
        super.init()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setBudgetButton(expenses: Double, income: Double) {
        let expenseString = NumberFormatter.localizedString(from: NSNumber.init(value: expenses.roundTo(places: 2)), number: NumberFormatter.Style.currency)
        let incomeString = NumberFormatter.localizedString(from: NSNumber.init(value: income.roundTo(places: 2)), number: NumberFormatter.Style.currency)
        self.budgetBtn.setTitle("Budget: \(expenseString) / \(incomeString)", for: .normal)
        if expenses > income {
            self.budgetBtn.setTitleColor(UIColor.black, for: .normal)
            self.budgetBtn.backgroundColor = UIColor.red
        } else {
            self.budgetBtn.setTitleColor(UIColor.white, for: .normal)
            self.budgetBtn.backgroundColor = UIColor.black
        }
    }
}
