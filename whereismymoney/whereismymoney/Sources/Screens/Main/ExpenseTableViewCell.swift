//
//  ProductTableViewCell.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 04.04.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import UIKit

class ExpenseTableViewCell: UITableViewCell {

    let container = UIStackView()
    let expenseName = UILabel()
    let expenseValue = UILabel()
    let categoryImage = UIImageView()
    let expenseDay = UILabel()
    
    let heightMultiplier = CGFloat(0.8)
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    private func setup() {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0)
        self.selectedBackgroundView = view
        self.tintColor = .appGreen
        
        setupStackView()
        setupDayLabel()
        setupCategoryImage()
        setupExpenseName()
        setupExpenseValue()
        self.setNeedsUpdateConstraints()
    }
    
    private func setupStackView() {
        container.translatesAutoresizingMaskIntoConstraints = false
        //self.addSubview(container)
        self.contentView.addSubview(container)
        container.widthAnchor.constraint(equalTo: self.contentView.widthAnchor, multiplier: 0.9).isActive = true
        container.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor).isActive = true
        container.topAnchor.constraint(equalTo: self.contentView.topAnchor).isActive = true
        container.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor).isActive = true
        container.spacing = 8
        container.distribution = .fill
        container.alignment = .center
        container.axis = .horizontal
        container.contentMode = .scaleToFill
    }
    
    private func setupDayLabel() {
        expenseDay.translatesAutoresizingMaskIntoConstraints = false
        container.addArrangedSubview(expenseDay)
        expenseDay.heightAnchor.constraint(equalTo: container.heightAnchor, multiplier: 0.9).isActive = true
        expenseDay.widthAnchor.constraint(equalTo: container.widthAnchor, multiplier: 0.07).isActive = true
        expenseDay.setContentHuggingPriority(UILayoutPriority(rawValue: 251), for: .horizontal)
        expenseDay.setContentHuggingPriority(UILayoutPriority(rawValue: 251), for: .vertical)
    }
    
    private func setupCategoryImage() {
        categoryImage.translatesAutoresizingMaskIntoConstraints = false
        container.addArrangedSubview(categoryImage)
        categoryImage.heightAnchor.constraint(equalTo: container.heightAnchor, multiplier: 0.4).isActive = true
        categoryImage.widthAnchor.constraint(equalTo: container.heightAnchor, multiplier: 0.4).isActive = true
        categoryImage.setContentHuggingPriority(UILayoutPriority(rawValue: 251), for: .horizontal)
        categoryImage.setContentHuggingPriority(UILayoutPriority(rawValue: 251), for: .vertical)
    }
    
    private func setupExpenseName() {
        expenseName.translatesAutoresizingMaskIntoConstraints = false
        container.addArrangedSubview(expenseName)
        expenseName.heightAnchor.constraint(equalTo: container.heightAnchor, multiplier: heightMultiplier).isActive = true
        expenseName.setContentHuggingPriority(UILayoutPriority(rawValue: 249), for: .horizontal)
        expenseName.setContentHuggingPriority(UILayoutPriority(rawValue: 249), for: .vertical)
        expenseName.textAlignment = .right
    }
    
    private func setupExpenseValue() {
        expenseValue.translatesAutoresizingMaskIntoConstraints = false
        container.addArrangedSubview(expenseValue)
        expenseValue.heightAnchor.constraint(equalTo: container.heightAnchor, multiplier: heightMultiplier).isActive = true
        expenseValue.widthAnchor.constraint(equalTo: container.widthAnchor, multiplier: 0.3).isActive = true
        expenseValue.textAlignment = .right
        expenseValue.setContentHuggingPriority(UILayoutPriority(rawValue: 251), for: .horizontal)
        expenseValue.setContentHuggingPriority(UILayoutPriority(rawValue: 251), for: .vertical)
    }
    
    func configureCell(expense: Expense, _ row: Int)
    {
        self.expenseDay.text = String(format: "%02d", expense.getDay())
        self.expenseName.text = expense.name
        //self.expenseValue.text = NumberFormatter.localizedString(from: NSNumber.init(value: expense.value), number: NumberFormatter.Style.currency)
        self.expenseValue.text = expense.value.asLocaleCurrency
        
        let image = UIImage(named: "\(expense.category.name).png")
        self.categoryImage.image = image
        if row % 2 == 1 {
            self.backgroundColor = UIColor.appYellow
        } else {
            self.backgroundColor = .white
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
