//
//  MainVC.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 04.04.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import UIKit
import CoreData
import FirebaseAuth

class MainVM: ScreenViewModel {
    
    // MARK: Closures
    var onDataUpdated: EmptyClosure?
    
    // MARK: Properties
    var budgetPeriod: BudgetPeriod?
    var year: Int
    var month: Int
    
    // MARK: Computed Properties
    var expenses: [Expense] {
        guard let budgetPeriod = budgetPeriod else { return [] }
        return budgetPeriod.expenses
    }
    
    var expensesValue: Double {
        guard let budgetPeriod = budgetPeriod else { return 0 }
        return budgetPeriod.calculateExpenses()
    }
    
    var incomeValue: Double {
        guard let budgetPeriod = budgetPeriod else { return 0 }
        return budgetPeriod.calculateIncome()
    }
    
    // MARK: Services
    var dataService: DataService?
    
    
    
    init(year: Int, month: Int) {
        self.year = year
        self.month = month
    }
    
    deinit {
        dataService?.stopObserving()
    }
    
    func startObserving() {
        guard let user = Auth.auth().currentUser else { return }
        dataService?.setBudgetForUser(user: user) { [weak self] in
            self?.dataService?.loadUserSettings { [weak self] (_, _) in
                self?.dataService?.loadCategoriesData(completionHandler: { [weak self] in
                    self?.startBudgetPeriodObserving()
                })
            }
        }
    }
    
    private func startBudgetPeriodObserving() {
        dataService?.observeBudgetPeriodForPeriod(year: year, month: month, completionHandler: { [weak self] (bp: BudgetPeriod) in
            self?.budgetPeriod = bp
            self?.onDataUpdated?()
        })
    }
    
    func getNextMonth() {
        if month == 12 {
            month = 1
            year += 1
        }else{
            month += 1
        }
        startBudgetPeriodObserving()
    }
    
    func getPrevMonth() {
        if month == 1 {
            month = 12
            year -= 1
        }else{
            month -= 1
        }
        startBudgetPeriodObserving()
    }
    
    func remove(expense: Expense) {
        dataService?.removeExpense(expense: expense)
    }
}

class MainVC: UIViewController {
    
    var navigationBarTitleButton = UIButton()

    let currentDate = Date()
    
    //var incomeTableView: UITableView!
    
    var model: MainVM!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        setupViewModel()
        setupCustomView()
        setupNavigationBar()
        setupGestures()
        
//        if let user = Auth.auth().currentUser {
//            DataService.shared.setBudgetForUser(user: user, completionHandler: {
//                self.loadData()
//            })
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func viewDidAppear(_ animated: Bool) {
//        guard Auth.auth().currentUser != nil else {
//            self.budgetPeriod?.clearData()
//            self.customView.table.reloadData()
//            let vc = LoginVC()
//            vc.mainVC = self
//            self.navigationController?.pushViewController(vc, animated: true)
//            return
//        }
    }
    
    
    func refreshViewsAfterSortChange(){
//        if let bp = self.budgetPeriod {
//            bp.sortExpenses(order: SettingsService.instance.expenseSort)
//            self.customView.table.reloadData()
//        }
    }
    
    func refreshIncomeViewsAfterSortChange(){
//        if let bp = self.budgetPeriod {
//            bp.sortIncomes(order: SettingsService.instance.incomeSort)
//            //self.incomeTableView.reloadData()
//        }
    }
    
    func currentPeriodAsString() -> String{
        return "\(model.month) / \(model.year)"
    }
    
    func updateBudgetLabel(){
        customView.setBudgetButton(expenses: model.expensesValue, income: model.incomeValue)
    }
    
    func updateNavigationBarTitle(){
        self.navigationBarTitleButton.setTitle(currentPeriodAsString(), for: .normal)
    }
}

// MARK: - Setup
extension MainVC {
    
    func setupViewModel() {
        
        model.onDataUpdated = { [weak self] in
            self?.updateBudgetLabel()
            self?.updateNavigationBarTitle()
            self?.customView.table.reloadData()
//            if let it = self?.incomeTableView {
//                it.reloadData()
//            }
            self?.customView.hideSpinner {
                self?.customView.showTable()
            }
        }
        
        model.startObserving()
    }
}
    
// MARK: - SetupUI
extension MainVC {
    
    func setupNavigationBar() {
        //title button
        self.navigationBarTitleButton =  UIButton(type: .custom)
        self.navigationBarTitleButton.frame = CGRect(x: 0, y: 0, width: 100, height: 40)
        self.navigationBarTitleButton.setTitleColor(UIColor.black, for: .normal)
        self.navigationBarTitleButton.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 22)
        self.navigationBarTitleButton.setTitle(currentPeriodAsString(), for: .normal)
        self.navigationBarTitleButton.addTarget(self, action: #selector(self.navigationBatTitleBtnPressed), for: .touchUpInside)
        self.navigationItem.titleView = self.navigationBarTitleButton
        self.navigationBarTitleButton.isEnabled = false
        
        //navigation bar colors
        self.navigationController?.navigationBar.backgroundColor = UIColor.appGreen
        self.navigationController?.navigationBar.tintColor = UIColor.appText
        self.navigationController?.navigationBar.barTintColor = UIColor.appGreen
        
        
        let settingsButton = UIButton(type: .system)
        settingsButton.setImage(UIImage(named: "ico_settings"), for: .normal)
        settingsButton.addTarget(self, action: #selector(self.settingsButtonPressed), for: .touchUpInside)
        
        let menuBarItem = UIBarButtonItem(customView: settingsButton)
        menuBarItem.customView?.translatesAutoresizingMaskIntoConstraints = false
        menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 22).isActive = true
        menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 22).isActive = true
        self.navigationItem.rightBarButtonItem = menuBarItem
        menuBarItem.isEnabled = false
    }
    
    func setupGestures() {
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipes(recognizer:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipes(recognizer:)))
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        customView.addGestureRecognizer(leftSwipe)
        customView.addGestureRecognizer(rightSwipe)
    }
}

// MARK: - Actions
extension MainVC {
    
    func goToNextPeriod() {
        model.getNextMonth()
    }
    
    func goToPrevPeriod() {
        model.getPrevMonth()
    }
    
    //TODO: Zrobic inaczej!
    func removeSelectedExpenses() {
        for selected in customView.table.indexPathsForSelectedRows ?? [] {
            model.remove(expense: model.expenses[selected.row])
        }
    }
    
    @objc func handleSwipes(recognizer: UIGestureRecognizer) {
        guard let swipeGesture = recognizer as? UISwipeGestureRecognizer else {
            return
        }
        if swipeGesture.direction == UISwipeGestureRecognizer.Direction.left {
            self.goToNextPeriod()
        }
        if swipeGesture.direction == UISwipeGestureRecognizer.Direction.right {
            self.goToPrevPeriod()
        }
    }
    
    @objc func navigationBatTitleBtnPressed(button: UIButton) {
//        if let bp = self.budgetPeriod, (bp.year != currentDate.year() || bp.month != currentDate.month()) {
//            self.getBudgetPeriod(year: currentDate.year(), month: currentDate.month())
//        }
    }
    
    func toolbarButtonPressed(button: UIBarButtonItem) {
        var targetVC: UIViewController?
        
        switch button.tag {
            case MainBottomTooblarButtonsEnum.ADD.rawValue:
                targetVC = ExpenseDetailsVC()
            case MainBottomTooblarButtonsEnum.STATS.rawValue:
                targetVC = StatsVC()
//                if let _ = budgetPeriod {
//                    (targetVC as! StatsVC).budgetPeriod = budgetPeriod
//                }
            case MainBottomTooblarButtonsEnum.DELETE.rawValue:
                if !customView.table.isEditing {
                    customView.table.setEditing(true, animated: true)
                } else {
                    customView.table.beginUpdates()
                    removeSelectedExpenses()
                    customView.table.setEditing(false, animated: true)
                    customView.table.reloadData()
                    customView.table.endUpdates()
                }
            default:
                return
        }
        if let vc = targetVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

// MARK: - Navigation
extension MainVC {
    
    func goToExpenseDetail(expense: Expense? = nil) {
        let flowManager = FlowManager()
        let modelData = ExpenseDetailVMData()
        modelData.expense = expense
        modelData.currentYear = model.year
        modelData.currentMonth = model.month
        _ = flowManager.navigate(from: self, to: .expenseDetail, with: modelData)
    }
    
    func goToIncomeList() {
        //        let vc = IncomeListVC()
        //        if let _ = budgetPeriod {
        //            vc.budgetPeriod = budgetPeriod
        //            vc.mainVC = self
        //        }
        //        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func settingsButtonPressed() {
        let vc = SettingsVC()
        vc.mainVC = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: -
extension MainVC: AppViewWithTableDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !customView.table.isEditing {
            let expense = model.expenses[indexPath.row]
            goToExpenseDetail(expense: expense)
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if let bp = self.budgetPeriod {
//            return bp.expenses.count
//        }
//        return 0
        return model.expenses.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let superviewHeight = self.view.superview?.frame.size.height else {
            return 30
        }
        return superviewHeight * 0.07
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExpenseCell", for: indexPath) as! ExpenseTableViewCell
        let index = indexPath.row
        let expense = model.expenses[index]
        cell.configureCell(expense: expense, index)
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, indentationLevelForRowAt indexPath: IndexPath) -> Int {
        return 30
    }
}

// MARK: - HasCustomView
extension MainVC: HasCustomView {
    
    typealias CustomView = MainView
    
    override func loadView() {
        view = MainView()
        self.edgesForExtendedLayout = []
    }
    
    func setupCustomView() {
        
        customView.onBudgetButtonTap = { [weak self] in
            self?.goToIncomeList()
        }
        
        customView.onToolbarButtonTap = { [weak self] buttonItem in
            self?.toolbarButtonPressed(button: buttonItem)
        }
        
        self.customView.delegate = self
        
        self.customView.configureToolbarButtons(MainBottomToolbarButtons())
        self.customView.configureTableCell(cellClass: ExpenseTableViewCell.self, cellReuseIdentifier: "ExpenseCell")
    }
}
