//
//  Toolbar.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 26/02/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import UIKit

enum MainBottomTooblarButtonsEnum: Int, CaseIterable {
    case STATS = 0
    case ADD = 1
    case DELETE = 2
}

struct MainBottomToolbarButtons: ToolbarButtons {
    let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
    func getButtons() -> [UIBarButtonItem] {
        var buttons = [UIBarButtonItem]()
        for b in MainBottomTooblarButtonsEnum.allCases {
            let button = UIBarButtonItem(image: UIImage(named: "ico_"+String(describing: b).lowercased()), style: .plain, target: nil, action: nil)
            button.tag = b.rawValue
            buttons.append(button)
            if b != MainBottomTooblarButtonsEnum.allCases.last {
                buttons.append(spacer)
            }
        }
        return buttons
    }
}
