//
//  ExportVC.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 20.12.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import UIKit
import WebKit
import GoogleMobileAds
import Firebase

class ExportVC: AdvertableVC, WKNavigationDelegate, HasCustomView {
    typealias CustomView = ExportView

    override func loadView() {
        view = CustomView()
        self.edgesForExtendedLayout = []
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customView.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func generateCSV(fromDate: Date, toDate: Date) {
        self.customView.disableViews {
            self.customView.showLoadingView()
            
            DataService.shared.getExpensesForPeriod(from: fromDate, to: toDate, expenses: [], incomes: []) {
                (expenses: [Expense], income: [Income]) in
                if !self.exportCancelled {
                    if let path = ExportService.instance.exportAsCsv(expenses: expenses, incomes: income, from: fromDate, to: toDate) {
                        if expenses.count > 0 || income.count > 0 {
                            let vc = UIActivityViewController(activityItems: [path], applicationActivities: [])
                            self.present(vc, animated: true, completion: nil)
                        } else {
                            self.showNoDataAlert()
                        }
                    }
                }
                self.customView.hideLoadingView {
                    self.customView.enableViews()
                    self.exportCancelled = false
                }
            }
        }
    }

    var exportCancelled = false
    
    @objc func cancel() {
        exportCancelled = true
        self.customView.hideLoadingView {
            self.customView.enableViews()
        }
    }
    
    func generatePDF(fromDate: Date, toDate: Date) {
        self.customView.disableViews {
            self.customView.showLoadingView()
            DataService.shared.getExpensesForPeriod(from: fromDate, to: toDate, expenses: [], incomes: []) {
                (expenses: [Expense], income: [Income]) in
                if !self.exportCancelled {
                    if expenses.count > 0 || income.count > 0 {
                        let html = HTMLReportFactory.instance.create(expenses: expenses, incomes: income, from: fromDate, to: toDate)
                        self.customView.dummyWebView.loadHTMLString(html, baseURL: nil)
                    } else {
                        self.showNoDataAlert()
                    }
                }
                self.customView.hideLoadingView {
                    self.customView.enableViews()
                    self.exportCancelled = false
                }
            }
        }
    }
    
    func showNoDataAlert() {
        let titleFont = [NSAttributedString.Key.font: UIFont(name: "Helvetica Neue", size: 15.0)!]
        let messageFont = [NSAttributedString.Key.font: UIFont(name: "Helvetica Neue", size: 11.0)!]
        let titleAttrString = NSMutableAttributedString(string: "No data to export!", attributes: titleFont)
        let messageAttrString = NSMutableAttributedString(string: "There is no data in selected period of time!", attributes: messageFont)
        let defaultAction = UIAlertAction(title: "OK", style: .default)
        let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
        alert.setValue(titleAttrString, forKey: "attributedTitle")
        alert.setValue(messageAttrString, forKey: "attributedMessage")
        alert.addAction(defaultAction)
        alert.view.tintColor = UIColor.appGreen
        alert.view.layer.cornerRadius = 40
        self.present(alert, animated: true) {}
    }

    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!)
    {
        let fromDate = self.customView.fromDatePicker.date.convertToLocalTime()
        let toDate = self.customView.toDatePicker.date.convertToLocalTime()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let fileName = "wimm_export_\(dateFormatter.string(from: fromDate))_to_\(dateFormatter.string(from: toDate)).pdf"
        if let path = ExportService.instance.exportAsPDF(webView, fileName) {
            let vc = UIActivityViewController(activityItems: [path], applicationActivities: [])
            self.present(vc, animated: true, completion: nil)
        }
        customView.hideSpinner()
    }
}
