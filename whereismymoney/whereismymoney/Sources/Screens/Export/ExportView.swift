//
//  ExportView.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 18/03/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import UIKit
import WebKit

class ExportView: UIViewWithSpinner {
    private var didSetupConstraints = false
    
    private weak var _delegate: ExportVC?
    
    var delegate: ExportVC? {
        get {
            if let _ = _delegate {
                return _delegate
            }
            return nil
        }
        set {
            _delegate = newValue
            dummyWebView.navigationDelegate = newValue
            
        }
    }
    
    let currentDate = Date()
    
    var dummyWebView = WKWebView()
    
    var fromDatePicker = UIDatePicker()
    var toDatePicker =  UIDatePicker()
    
    var dateStackView = UIStackView()
    
    var btnsStackView = UIStackView()
    
    var loaderView = UIStackView()
    
    var wrapper = UIStackView()
    
    lazy var cancelButton = UIButton()
    
    lazy var loadingView = UIView()
    
    override init(){
        super.init()
        self.setup()
    }
    
    private func setup() {
        self.backgroundColor = .white
        setupWrapper()
        setupExportLabel()
        setupDatePickers()
        setupExportButtons()
        setCurrentMonthDate()
        setupLoadingView()
        self.spinner.isHidden = true
    }
    
    private func setupWrapper() {
        self.addSubview(wrapper)
        wrapper.translatesAutoresizingMaskIntoConstraints = false
        wrapper.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.9).isActive = true
        wrapper.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.9).isActive = true
        wrapper.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        wrapper.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        wrapper.axis = .vertical
        wrapper.contentMode = .scaleAspectFit
        wrapper.distribution = .fill
        wrapper.spacing = 0
        wrapper.alignment = .center
        
    }
    
    private func setupExportLabel() {
        let label = UILabel()
        label.text = "Data Export"
        label.textAlignment = .center
        label.contentMode = .scaleAspectFit
        self.wrapper.addArrangedSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.widthAnchor.constraint(equalTo: wrapper.widthAnchor).isActive = true
        label.heightAnchor.constraint(equalTo: wrapper.heightAnchor, multiplier: 0.1).isActive = true
    }
    
    private func setupDatePickers() {
        let dataPickersWrapper = UIStackView()
        wrapper.addArrangedSubview(dataPickersWrapper)
        dataPickersWrapper.translatesAutoresizingMaskIntoConstraints = false
        dataPickersWrapper.widthAnchor.constraint(equalTo: wrapper.widthAnchor, multiplier: 1).isActive = true
        dataPickersWrapper.heightAnchor.constraint(equalTo: wrapper.heightAnchor, multiplier: 0.6).isActive = true
        dataPickersWrapper.axis = .vertical
        dataPickersWrapper.contentMode = .scaleAspectFit
        dataPickersWrapper.distribution = .equalSpacing
        dataPickersWrapper.spacing = 10
        dataPickersWrapper.alignment = .center
        
        let periodsPickers = UIStackView()
        periodsPickers.axis = .horizontal
        periodsPickers.contentMode = .scaleAspectFit
        periodsPickers.distribution = .equalSpacing
        periodsPickers.spacing = 10
        periodsPickers.alignment = .center
        dataPickersWrapper.addArrangedSubview(periodsPickers)
        periodsPickers.translatesAutoresizingMaskIntoConstraints = false
        periodsPickers.widthAnchor.constraint(equalTo: dataPickersWrapper.widthAnchor, multiplier: 0.8)
            .isActive = true
        for buttonName in ["Month", "Quarter", "Year"] {
            let button = UIButton()
            button.setTitle(buttonName, for: .normal)
            button.setTitleColor(.appGreen, for: .normal)
            periodsPickers.addArrangedSubview(button)
            button.translatesAutoresizingMaskIntoConstraints = false
            button.heightAnchor.constraint(equalTo: wrapper.heightAnchor, multiplier: 0.1).isActive = true
            button.addTarget(self, action: Selector(buttonName.lowercased()+"Pressed"), for: .touchUpInside)
        }
        
        let datePickers = UIStackView()
        
        datePickers.axis = .vertical
        datePickers.contentMode = .scaleAspectFit
        datePickers.distribution = .equalSpacing
        datePickers.spacing = 0
        datePickers.alignment = .center
        dataPickersWrapper.addArrangedSubview(datePickers)
        datePickers.translatesAutoresizingMaskIntoConstraints = false
        datePickers.widthAnchor.constraint(equalTo: dataPickersWrapper.widthAnchor).isActive = true
        
        let fromLabel = UILabel()
        let toLabel = UILabel()
        
        datePickers.addArrangedSubview(fromLabel)
        datePickers.addArrangedSubview(fromDatePicker)
        datePickers.addArrangedSubview(toLabel)
        datePickers.addArrangedSubview(toDatePicker)
        
        
        for label in [("From", fromLabel), ("To", toLabel)] {
            label.1.text = label.0
            label.1.textColor = .appText
            label.1.translatesAutoresizingMaskIntoConstraints = false
            label.1.textAlignment = .left
            label.1.widthAnchor.constraint(equalTo: datePickers.widthAnchor).isActive = true
            label.1.heightAnchor.constraint(equalTo: datePickers.heightAnchor, multiplier: 0.1).isActive = true
        }
        
        for picker in [fromDatePicker, toDatePicker] {
            picker.datePickerMode = .date
            picker.widthAnchor.constraint(equalTo: datePickers.widthAnchor).isActive = true
            picker.heightAnchor.constraint(equalTo: datePickers.heightAnchor, multiplier: 0.40).isActive = true
        }
        fromDatePicker.addTarget(self, action: #selector(fromDateChanged(_:)), for: .valueChanged)
    }
    
    private func setupExportButtons() {
        let exportButtonsWrapper = UIView()
        exportButtonsWrapper.translatesAutoresizingMaskIntoConstraints = false
        wrapper.addArrangedSubview(exportButtonsWrapper)
        exportButtonsWrapper.widthAnchor.constraint(equalTo: wrapper.widthAnchor, multiplier: 1).isActive = true
        let exportButtons = UIStackView()
        exportButtons.axis = .horizontal
        exportButtons.contentMode = .scaleAspectFit
        exportButtons.distribution = .equalSpacing
        exportButtons.spacing = 10
        exportButtons.alignment = .center
        exportButtonsWrapper.addSubview(exportButtons)
        //wrapper.addArrangedSubview(exportButtons)
        exportButtons.translatesAutoresizingMaskIntoConstraints = false
        exportButtons.widthAnchor.constraint(equalTo: exportButtonsWrapper.widthAnchor, multiplier: 0.65).isActive = true
        exportButtons.heightAnchor.constraint(equalTo: wrapper.heightAnchor, multiplier: 0.11).isActive = true
        exportButtons.centerYAnchor.constraint(equalTo: exportButtonsWrapper.centerYAnchor).isActive = true
        exportButtons.centerXAnchor.constraint(equalTo: exportButtonsWrapper.centerXAnchor).isActive = true

        for buttonName in ["CSV", "PDF"] {
            let button = RoundButton()
            let image = UIImage(named: buttonName+"_50")
            button.setImage(image, for: .normal)
            button.backgroundColor = .appGreen
            button.addTarget(self, action: Selector(buttonName.lowercased()+"Pressed"), for: .touchUpInside)
            exportButtons.addArrangedSubview(button)
            button.translatesAutoresizingMaskIntoConstraints = false
            button.heightAnchor.constraint(equalTo: exportButtons.heightAnchor, multiplier: 1).isActive = true
            button.widthAnchor.constraint(equalTo: exportButtons.heightAnchor, multiplier: 1).isActive = true
        }
        
    }
    
    func setupLoadingView() {
        loadingView.isHidden = true
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(loadingView)
        loadingView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        loadingView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        loadingView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        loadingView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        loadingView.backgroundColor = UIColor(white: 0, alpha: 0.2)
        
//        let popup = UIView()
//        popup.translatesAutoresizingMaskIntoConstraints = false
//        loadingView.addSubview(popup)
//        popup.widthAnchor.constraint(equalTo: loadingView.widthAnchor, multiplier: 0.7).isActive = true
//        popup.heightAnchor.constraint(equalTo: loadingView.heightAnchor, multiplier: 0.25).isActive = true
//        popup.centerXAnchor.constraint(equalTo: loadingView.centerXAnchor).isActive = true
//        popup.centerYAnchor.constraint(equalTo: loadingView.centerYAnchor).isActive = true
//        popup.backgroundColor = .white
//        popup.layer.cornerRadius = 20.0
//
//        let stackView = UIStackView()
//        stackView.translatesAutoresizingMaskIntoConstraints = false
//        popup.addSubview(stackView)
//        stackView.widthAnchor.constraint(equalTo: popup.widthAnchor, multiplier: 1).isActive = true
//        stackView.heightAnchor.constraint(equalTo: popup.heightAnchor, multiplier: 0.80).isActive = true
//        stackView.centerXAnchor.constraint(equalTo: popup.centerXAnchor).isActive = true
//        stackView.centerYAnchor.constraint(equalTo: popup.centerYAnchor).isActive = true
//        stackView.axis = .vertical
//        stackView.contentMode = .scaleAspectFit
//        stackView.distribution = .equalSpacing
//        stackView.spacing = 0
//        stackView.alignment = .center
//
//        let label = UILabel()
//        label.numberOfLines = 1
//        label.textAlignment = .center
//        label.text = "Preparing export"
//        label.translatesAutoresizingMaskIntoConstraints = false
//        stackView.addArrangedSubview(label)
//        label.heightAnchor.constraint(equalTo: stackView.heightAnchor, multiplier: 0.2).isActive = true
//
//
        self.spinner = UIActivityIndicatorView(style: .gray)
        spinner.translatesAutoresizingMaskIntoConstraints = false
        loadingView.addSubview(spinner)
        //stackView.addArrangedSubview(spinner)
        //spinner.widthAnchor.constraint(equalTo: stackView.widthAnchor, multiplier: 0.2).isActive = true
        //spinner.heightAnchor.constraint(equalTo: spinner.widthAnchor, multiplier: 1).isActive = true
        spinner.widthAnchor.constraint(equalTo: loadingView.widthAnchor, multiplier: 0.2).isActive = true
        spinner.heightAnchor.constraint(equalTo: spinner.widthAnchor, multiplier: 1).isActive = true
        spinner.centerYAnchor.constraint(equalTo: loadingView.centerYAnchor).isActive = true
        spinner.centerXAnchor.constraint(equalTo: loadingView.centerXAnchor).isActive = true
        spinner.isHidden = false
        
        
//
//        let separator = SeparatorView()
//        separator.backgroundColor = .lightGray
//        stackView.addArrangedSubview(separator)
        
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        loadingView.addSubview(cancelButton)
        cancelButton.widthAnchor.constraint(equalTo: loadingView.widthAnchor, multiplier: 1).isActive = true
        cancelButton.heightAnchor.constraint(equalTo: loadingView.heightAnchor, multiplier: 0.2).isActive = true
        cancelButton.centerXAnchor.constraint(equalTo: loadingView.centerXAnchor).isActive = true
        cancelButton.centerYAnchor.constraint(equalTo: loadingView.centerYAnchor, constant: 200).isActive = true
        cancelButton.setTitleColor(.appRed, for: .normal)
        cancelButton.setTitle("Cancel", for: .normal)
        cancelButton.addTarget(self.delegate, action: #selector(self.delegate?.cancel), for: .touchUpInside)
        self.bringSubviewToFront(loadingView)
    }
    
    
    @objc private func monthPressed() {
        setCurrentMonthDate()
    }
    
    @objc private func quarterPressed() {
        setCurrentQuarterDate()
    }
    
    @objc private func yearPressed() {
        setCurrentYearDate()
    }

    @objc private func csvPressed() {
        let fromDate = fromDatePicker.date.convertToLocalTime()
        let toDate = toDatePicker.date.convertToLocalTime()
        self.delegate?.generateCSV(fromDate: fromDate, toDate: toDate)
    }
    
    @objc private func pdfPressed() {
        let fromDate = fromDatePicker.date.convertToLocalTime()
        let toDate = toDatePicker.date.convertToLocalTime()
        self.delegate?.generatePDF(fromDate: fromDate, toDate: toDate)
    }
    
    private func setCurrentMonthDate() {
        if let startOfMonth = currentDate.startOfMonth() {
            self.fromDatePicker.date = startOfMonth
        }
        if let endOfMonth = currentDate.endOfMonth() {
            self.toDatePicker.date = endOfMonth
        }
    }
    
    private func setCurrentQuarterDate() {
        if let startOfQuarter = currentDate.startOfQuarter() {
            self.fromDatePicker.date = startOfQuarter
        }
        if let endOfQuarter = currentDate.endOfQuarter() {
            self.toDatePicker.date = endOfQuarter
        }
    }
    
    private func setCurrentYearDate() {
        if let startOfYear = currentDate.startOfYear() {
            self.fromDatePicker.date = startOfYear
        }
        if let endOfYear = currentDate.endOfYear() {
            self.toDatePicker.date = endOfYear
        }
    }
    
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }

    
    func showLoadingView() {
        self.loadingView.alpha = 0
        self.spinner.startAnimating()
        self.loadingView.isHidden = false
        UIView.animate(withDuration: 0.3, animations: {
                 self.loadingView.alpha = 1
            }, completion: nil)
    }
    
    func hideLoadingView(post: @escaping (()->()) = {}) {
        UIView.animate(withDuration: 0.1, animations: {
            self.loadingView.alpha = 0
            self.loadingView.isHidden = true
            self.spinner.stopAnimating()
            }, completion:  { (value: Bool) in
                post()
        })
    }
    
    func disableViews(post: @escaping (()->()) = {}) {
        self.wrapper.isUserInteractionEnabled = false
        UIView.animate(withDuration: 0.1, animations: {
                 self.wrapper.alpha = 0.3
            }, completion:  { (value: Bool) in
                post()
        })
    }
    
    func enableViews() {
        self.wrapper.isUserInteractionEnabled = true
        UIView.animate(withDuration: 0.1, animations: {
                 self.wrapper.alpha = 1
            }, completion:  { (value: Bool) in
        })
    }
    
    @objc func fromDateChanged(_ sender: Any) {
        toDatePicker.minimumDate = fromDatePicker.date
    }
    
}
