//
//  AboutVC.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 01.03.2018.
//  Copyright © 2018 Piotr Figiel. All rights reserved.
//

import UIKit
import GoogleMobileAds
import WebKit


class AboutVC: UIViewController, WKNavigationDelegate, HasCustomView {

    typealias CustomView = AboutView

    override func loadView() {
        view = AboutView()
        self.edgesForExtendedLayout = []
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func generateDataTouched(_ sender: Any) {
        let fakeDataService = FakeDataService.instance
        fakeDataService.createFakeDataForYear(year: 2019)
    }
}
