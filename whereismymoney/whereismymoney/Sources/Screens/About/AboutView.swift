//
//  AboutView.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 14/03/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import UIKit
import WebKit


class AboutView: UIView, WKNavigationDelegate {

    private var didSetupConstraints = false
    
    weak var delegate: StatsVC?
    
    var wrapper = UIStackView()
    
    var text = UITextView()
    
    var logo = UIImageView()
    
    
    init(){
        super.init(frame: CGRect.zero)
        self.setup()
    }
    
    private func setup() {
        self.backgroundColor = .white
        clipsToBounds = true
        setupWrapper()
        setupBackgroundImage()
        setupLogo()
        setupText()
        self.setNeedsUpdateConstraints()
    }
    
    private func setupWrapper() {
        self.addSubview(wrapper)
        wrapper.translatesAutoresizingMaskIntoConstraints = false
        wrapper.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.9).isActive = true
        wrapper.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.9).isActive = true
        wrapper.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        wrapper.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        
        wrapper.alignment = .center
        wrapper.distribution = .fill
        wrapper.axis = .vertical
    }
    
    private func setupBackgroundImage() {
        let background = UIImage(named: "bg")
        var imageView : UIImageView!
        imageView = UIImageView(frame: self.bounds)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = self.center
        self.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        imageView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        imageView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        imageView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        self.sendSubviewToBack(imageView)
        self.backgroundColor = UIColor.appGreen
    }
    
    private func setupLogo(){
        wrapper.addArrangedSubview(logo)
        logo.translatesAutoresizingMaskIntoConstraints = false
        let image = UIImage(named: "pig_logo_green.png")
        logo.image = image
        logo.widthAnchor.constraint(equalTo: wrapper.widthAnchor, multiplier: 0.6).isActive = true
        logo.heightAnchor.constraint(equalTo: wrapper.widthAnchor, multiplier: 0.6).isActive = true
    }
    
    private func setupText() {
        wrapper.addArrangedSubview(text)
        text.heightAnchor.constraint(equalTo: wrapper.heightAnchor, multiplier: 0.5).isActive = true
        text.widthAnchor.constraint(equalTo: wrapper.widthAnchor, multiplier: 1).isActive = true
        text.isUserInteractionEnabled = false
        
        text.backgroundColor = .none
        text.textColor = .white
        text.tintColor = .white
        
        let txt = "This is the simplest app out there\nto manage your expenses.\nPlease send your comments, suggestions,\nbugs and problems to: support@whereismymoney.com\n\nwww.whereismymoney.com\n\n\n\nIcons for expenses view made by\nAlexandru Stoica\nIcons on export view made by Freepik from www.flaticon.com is licensed by CC 3.0 BY\n\nIcons for stats menu made by \nFreepik\n from www.flaticon.com is licensed by CC 3.0BY"
        
        let attributedString = NSMutableAttributedString(string: txt)
        attributedString.addAttribute(.link, value: "https://dribbble.com/shots/2888226-1800-Free-Minimal-Icon-Pack-20x20", range: (txt as NSString).range(of: "Alexandru Stoica"))
        attributedString.addAttribute(.link, value: "http://creativecommons.org/licenses/by/3.0/", range: (txt as NSString).range(of: "CC 3.0 BY"))
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white , range: (txt as NSString).range(of: txt))
        attributedString.addAttribute(.link, value: "http://www.freepik.com", range: (txt as NSString).range(of: "Freepik"))
        text.attributedText = attributedString
        text.textAlignment = .center
        
    }

    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        UIApplication.shared.open(URL, options: [:])
        return false
    }
}
