//
//  StatsControllerViewController.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 26.06.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import UIKit
import Charts

class StatsVC: UIViewController, HasCustomView {
    typealias CustomView = StatsView

    override func loadView() {
        view = CustomView()
        self.edgesForExtendedLayout = []
    }
    
    var months: [String]!
    weak var budgetPeriod: BudgetPeriod!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.customView.delegate = self
        self.showCurrentMonthStats()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func showCurrentMonthStats(){
        if let bp = self.budgetPeriod {
            let month = DateFormatter().monthSymbols[bp._month-1]
            let year = bp.year
            let data = StatsService.instance.getStatsFor(budgetPeriod: bp, true, true)
            let expensesValue = bp.calculateExpenses()
            let incomeValue = bp.calculateIncome()
            self.customView.showMonthStats(month, String(year), data, expensesValue, incomeValue)
        }
    }
    
    func monthButtonPressed() {
        showCurrentMonthStats()
    }
    
    func quarterBtnPressed() {
        if let bp = self.budgetPeriod {
            let formater = NumberFormatter()
            formater.numberStyle = .ordinal
            let quarter = formater.string(from:  NSNumber(value: Date.getQuarterFrom(month: bp._month))) ?? String(bp.month)
            StatsService.instance.getQuarterlyCategoryExpensesAccordingToBudgetPeriod(budgetPeriod: bp) {
                (expenseStats: Dictionary<String, Double>, incomeOverall: Double, expenseOverall: Double) in
                        self.customView.showQuarterStats(quarter: quarter, year: bp.year, expenseStats, income: incomeOverall, expenses: expenseOverall)
            }
        }
    }

    func yearBtnPressed() {
        if let bp = self.budgetPeriod {
            StatsService.instance.getYearlyCategoryExpensesAccordingToBudgetPeriod(budgetPeriod: bp) {
                (expenseStats: Dictionary<String, Double>, incomeOverall: Double, expenseOverall: Double) in
                    self.customView.showYearStats(year: bp.year, expenseStats, income: incomeOverall, expenses: expenseOverall)
            }
        }
    }
    
    func eiBtnPressed() {
        if let bp = self.budgetPeriod {
            StatsService.ss.getYearlyIcomeExpenseAccordingToBudgetPeriod(budgetPeriod: bp) {
                (expenses: Dictionary<Int, Double>, income: Dictionary<Int, Double>) in
                    self.customView.showEIChart(bp.year, expenses: expenses, income: income)
            }
        }
    }
}
