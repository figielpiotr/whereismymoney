//
//  StatsView.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 14/03/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import UIKit
import Charts

class StatsView: UIViewWithSpinner {

    private var didSetupConstraints = false
    
    weak var delegate: StatsVC?
    
    var wrapper = UIView()
    
    var pieChartView = PieChartView()
    
    var lineChartView = LineChartView()
    
    var budgetLbl: UILabel!
    
    var loadSpinner: UIActivityIndicatorView!
    
    let toolbar = UIToolbar()
    
    override init(){
       super.init()
       self.setup()
    }
    
    private func setup() {
        self.backgroundColor = .white
        clipsToBounds = true
        setupToolbar()
        setupWrapper()
        setupChartViews()
        
        self.setNeedsUpdateConstraints()
    }
    
    private func setupWrapper() {
        self.addSubview(wrapper)
        wrapper.translatesAutoresizingMaskIntoConstraints = false
        wrapper.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.9).isActive = true
        wrapper.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        wrapper.bottomAnchor.constraint(equalTo: toolbar.topAnchor).isActive = true
        wrapper.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        wrapper.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
    }
    
    @objc func monthButtonPressed() {
        self.hideView(pieChartView) {
            self.hideView(self.lineChartView) {
                if let _ = self.delegate {
                    self.delegate?.monthButtonPressed()
                }
            }
        }
    }
    
    @objc func quarterButtonPressed() {
        self.hideView(pieChartView) {
            self.hideView(self.lineChartView) {
                if let _ = self.delegate {
                    self.delegate?.quarterBtnPressed()
                }
            }
        }
    }
    
    @objc func yearButtonPressed() {
        self.hideView(pieChartView) {
            self.hideView(self.lineChartView) {
                if let _ = self.delegate {
                    self.delegate?.yearBtnPressed()
                }
            }
        }
    }
    
    @objc func eiButtonPressed() {
        self.hideView(pieChartView) {
            self.hideView(self.lineChartView) {
                if let _ = self.delegate {
                    self.delegate?.eiBtnPressed()
                }
            }
        }
    }
    
    private func setupToolbar() {
        self.addSubview(toolbar)
        toolbar.translatesAutoresizingMaskIntoConstraints = false
        toolbar.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        toolbar.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        
        if self.hasNotch {
            toolbar.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.08).isActive = true
            toolbar.bottomAnchor.constraint(equalTo: self.safeAreaBottomAnchor, constant: 0).isActive = true
        } else {
            toolbar.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.09).isActive = true
            toolbar.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        }
        
        
        
        toolbar.barTintColor = .appGreen
        toolbar.tintColor = .appText
        toolbar.backgroundColor = .appGreen
        
        let buttons = ["month", "quarter", "year", "ei"]
        var items = [UIBarButtonItem]()

        for name in buttons {
            let button = UIBarButtonItem(
                image: UIImage(named: "ico_stats_"+name),
                style: .plain, target: self,
                action: Selector(name+"ButtonPressed")
            )
            items.append(button)
            if name != buttons.last {
                items.append(UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil))
            }
        }
        toolbar.setItems(items, animated: true)
    }
    
    private func setupChartViews() {
        for chart in [lineChartView, pieChartView] {
            wrapper.addSubview(chart)
            chart.isHidden = true
            chart.translatesAutoresizingMaskIntoConstraints = false
            chart.leadingAnchor.constraint(equalTo: wrapper.leadingAnchor).isActive = true
            chart.trailingAnchor.constraint(equalTo: wrapper.trailingAnchor).isActive = true
            chart.topAnchor.constraint(equalTo: wrapper.topAnchor).isActive = true
            chart.bottomAnchor.constraint(equalTo: wrapper.bottomAnchor).isActive = true
        }
    }
    
    func showMonthStats(_ month: String, _ year: String,  _ data: Dictionary<String, Double>, _ expenseValue: Double, _ incomeValue: Double) {
        self.clearChart(self.pieChartView)
        self.pieChartView.noDataText = "No data for \(month) \(year)."
        self.pieChartView.chartDescription?.text = "Stats for \(month) of \(year)."
        self.setChart(data, income: incomeValue, expenses: expenseValue)
        self.showView(pieChartView)
    }
            
    
    func showQuarterStats(quarter: String, year: Int, _ data: Dictionary<String, Double>, income: Double? = nil, expenses: Double? = nil) {
        self.clearChart(self.pieChartView)
        self.pieChartView.noDataText = "No data for \(quarter) quarter of \(year)."
        self.pieChartView.chartDescription?.text = "Stats for \(quarter) quarter of \(year)."
        setChart(data, income: income, expenses: expenses)
        self.pieChartView.reloadInputViews()
        self.showView(pieChartView)
    }
    
    func showYearStats(year: Int, _ data: Dictionary<String, Double>, income: Double? = nil, expenses: Double? = nil) {
        self.clearChart(self.pieChartView)
        self.pieChartView.noDataText = "No data for \(year)."
        self.pieChartView.chartDescription?.text = "Stats for \(year)."
        setChart(data, income: income, expenses: expenses)
        self.pieChartView.reloadInputViews()
        self.showView(pieChartView)
    }
    
    func showEIChart(_ year: Int, expenses: Dictionary<Int, Double>, income: Dictionary<Int, Double>) {
        self.lineChartView.noDataText = "No data for \(year)."
        self.lineChartView.chartDescription?.text = "Income vs Expenses for \(year)."
        self.setLineChart(expenses: expenses, income: income)
        self.lineChartView.reloadInputViews()
        self.showView(lineChartView)
    }
    
    private func setChart(_ data: Dictionary<String, Double>, income: Double? = nil, expenses: Double? = nil) {
        var dataEntries: [ChartDataEntry] = []
        for (category, value) in data {
            if(value == 0) {
                continue
            }
            
            var image: UIImage?
            if value >= 6 {
                image = UIImage(named: "\(category)_26.png")
            }
            else if  value < 6 {
                image = UIImage(named: "\(category)_14.png")
            }

            let dataEntry = PieChartDataEntry(value: value, icon: image)
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = PieChartDataSet(entries: dataEntries, label: "")
        chartDataSet.drawValuesEnabled = false
        let chartData = PieChartData(dataSet: chartDataSet)
        
        chartDataSet.colors = UIColor.getGraphColors()
        chartDataSet.selectionShift = 0
        pieChartView.data = chartData

        if let e = expenses, let i = income {
            let currencySymbol = Locale.current.currencySymbol
            if i != 0 {
                pieChartView.centerText = "\(Int(e.rounded()))/\(Int(i.rounded())) \(currencySymbol!)"
            } else {
                pieChartView.centerText = "No data"
            }
            
        }
        pieChartView.legend.enabled = false
        pieChartView.rotationEnabled = false
        chartData.notifyDataChanged()
        chartDataSet.notifyDataSetChanged()
    }
    
    func clearChart(_ chart: ChartViewBase) {
        chart.data = nil
        chart.notifyDataSetChanged()
    }
    
    func formatLineDataSet(_ set: inout LineChartDataSet, color: UIColor?) {
        set.lineWidth = 2.0
        set.circleRadius = 4.0
        set.drawValuesEnabled = false
        set.drawFilledEnabled = true
        
        set.fillAlpha = 0.3
        if let c = color {
            set.colors = [c]
            set.circleColors = [c]
            set.fillColor = c
        }
    }
    
    func setLineChart(expenses: Dictionary<Int, Double>, income: Dictionary<Int, Double>) {
        var expenseChartEntry  = [ChartDataEntry]()
        var incomeChartEntry  = [ChartDataEntry]()
        
        for i in 1...12 {
            let entry = ChartDataEntry(x: Double(i), y: expenses[i]!)
            expenseChartEntry.append(entry)
        }
        
        for i in 1...12 {
            let entry = ChartDataEntry(x: Double(i), y: income[i]!)
            incomeChartEntry.append(entry)
        }
        
        var incomeLine = LineChartDataSet(entries: incomeChartEntry, label: "Income")
        formatLineDataSet(&incomeLine, color: UIColor.appGreen)
        var expenseLine = LineChartDataSet(entries: expenseChartEntry, label: "Expense")
        formatLineDataSet(&expenseLine, color: UIColor.appText)
        let data = LineChartData()
        data.addDataSet(incomeLine)
        data.addDataSet(expenseLine)
        lineChartView.data = data
        lineChartView.noDataText = ""
        lineChartView.xAxis.axisMinimum = incomeLine.xMin
        lineChartView.xAxis.axisMaximum = incomeLine.xMax
        lineChartView.xAxis.avoidFirstLastClippingEnabled = true
        lineChartView.xAxis.granularity = 1.0
        lineChartView.xAxis.granularityEnabled = true
        lineChartView.xAxis.setLabelCount(12, force: true)
        lineChartView.leftAxis.axisMinimum = 0.0
        lineChartView.leftAxis.granularityEnabled = true
        lineChartView.leftAxis.granularity = 10.0
        lineChartView.rightAxis.enabled = false
    }
    
    
    func showView(_ view: UIView){
        if view.isHidden {
            view.isHidden = false
            UIView.animate(withDuration: 0.1, delay: 0.2, options: .curveEaseInOut,
                           animations: {view.alpha = 1},
                           completion: { _ in
                            self.hideSpinner()
            })
        }
    }
    
    func hideView(_ view: UIView, completion: @escaping () -> Void) {
        if !view.isHidden {
            UIView.animate(withDuration: 0.1, delay: 0.2, options: .curveEaseInOut,
                           animations: {view.alpha = 0},
                           completion: { _ in
                            view.isHidden = true
                            self.showSpinner()
                            completion()
            })
        } else {
            self.showSpinner()
            completion()
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
}
