//
//  AccountVC.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 07.04.2018.
//  Copyright © 2018 Piotr Figiel. All rights reserved.
//

import UIKit
import Firebase

class AccountVC: UIViewController, UITextFieldDelegate, HasCustomView {

    typealias CustomView = AccountView

    override func loadView() {
        view = CustomView()
        self.edgesForExtendedLayout = []
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customView.delegate = self
        setLoginInfo()
        self.hideKeyboardWhenTappedAround()

    }

    func setLoginInfo() {
        if let user = Auth.auth().currentUser {
            if let provider = AuthService.instance.getLoginProvider() {
                var providerLabel = ""
                if let email = user.email, provider != FIR_AUTH_PROVIDER_TW {
                    providerLabel = email
                } else {
                    providerLabel = FIR_AUTH_PROVIDER_TW
                }

                self.customView.setLoginData(label: providerLabel, provider: provider)
            }
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    func logoutPressed() {        
        AuthService.instance.logout { (error, _) in
            if error != nil {
                let alert = UIAlertController(title: "Sign Out Problem", message: "Could not sign out. Please try again!", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
        }
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    func changePasswordPressed(oldPassword: String, newPassword: String) {        
        AuthService.instance.changePasswordForCurrentUser(oldPassword, newPassword) { (error, _) in
            if let error = error {
                self.showAlert("A problem occured", "\(error).", [UIAlertAction(title: "OK", style: .cancel)])
            } else {
                self.showAlert("Password changed", "Password was successfully changed.", [UIAlertAction(title: "OK", style: .default)])
                self.customView.resetPasswordInputs()
            }
        }
    }
    
    
}
