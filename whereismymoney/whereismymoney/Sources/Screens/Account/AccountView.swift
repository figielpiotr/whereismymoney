//
//  AccountView.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 13/03/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import UIKit

class AccountView: UIView {
    private var didSetupConstraints = false
       
    weak var delegate: AccountVC?

    let wrapper = UIStackView()
    
    let loggedAsLabel = UILabel()
    var emailLabel = UILabel()
    let providerImage = UIImageView()
    let logoutButton = RoundCornersUIButton()
    
    let passwordStackView = UIStackView()
    let oldPasswordInput = ColorPasswordField()
    let newPasswordInput = ColorPasswordField()
    let repeatPasswordInput = ColorPasswordField()
    let changePasswordButton = RoundCornersUIButton()
        
    let buttonHeight = CGFloat(0.08)
    let textInputHeight = CGFloat(0.08)
    let wrapperHeight = CGFloat(0.75)
    
    init(){
       super.init(frame: CGRect.zero)
       self.setup()
    }
    
    private func setup() {
        self.backgroundColor = .white
        clipsToBounds = true
        setupStackView()
        setupLoginInfo()
        setupChangePassword()
        setupButtonActions()
        self.setNeedsUpdateConstraints()
    }
    
        

    private func setupStackView() {
       self.addSubview(wrapper)
       wrapper.translatesAutoresizingMaskIntoConstraints = false
       wrapper.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.9).isActive = true
       wrapper.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: wrapperHeight).isActive = true
       wrapper.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
       wrapper.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
       wrapper.axis = .vertical
       wrapper.contentMode = .scaleAspectFit
       wrapper.distribution = .equalSpacing
       wrapper.spacing = 10
       wrapper.alignment = .center
    }
       
    private func setupLoginInfo() {
        let loginInfoStackView = UIStackView()
        wrapper.addArrangedSubview(loginInfoStackView)
        loginInfoStackView.translatesAutoresizingMaskIntoConstraints = false
        loginInfoStackView.widthAnchor.constraint(equalTo: wrapper.widthAnchor, multiplier: 1).isActive = true
        //loginInfoStackView.heightAnchor.constraint(equalTo: wrapper.heightAnchor, multiplier: 0.25).isActive = true
        loginInfoStackView.axis = .vertical
        loginInfoStackView.contentMode = .scaleAspectFit
        loginInfoStackView.distribution = .equalSpacing
        loginInfoStackView.spacing = 10
        loginInfoStackView.alignment = .leading
        
        let infoStackView = UIStackView()
        loginInfoStackView.addArrangedSubview(infoStackView)
        infoStackView.translatesAutoresizingMaskIntoConstraints = false
        infoStackView.widthAnchor.constraint(equalTo: loginInfoStackView.widthAnchor, multiplier: 1).isActive = true
        infoStackView.axis = .horizontal
        infoStackView.contentMode = .scaleAspectFit
        infoStackView.distribution = .equalSpacing
        infoStackView.spacing = 0
        infoStackView.alignment = .center
        
        loggedAsLabel.text = "Logged As: "
        loggedAsLabel.translatesAutoresizingMaskIntoConstraints = false
        infoStackView.addArrangedSubview(loggedAsLabel)
        
        emailLabel.text = "test@test.com"
        emailLabel.translatesAutoresizingMaskIntoConstraints = false
        infoStackView.addArrangedSubview(emailLabel)
        
        
        let image = UIImage(named: "password")
        providerImage.image = image
        providerImage.translatesAutoresizingMaskIntoConstraints = false
        infoStackView.addArrangedSubview(providerImage)
        providerImage.heightAnchor.constraint(equalTo: infoStackView.heightAnchor, multiplier: 1).isActive = true
        providerImage.widthAnchor.constraint(equalTo: providerImage.heightAnchor, multiplier: 1).isActive = true
        
        logoutButton.setTitle("LOG OUT", for: .normal)
        logoutButton.setTitleColor(.white, for: .normal)
        logoutButton.backgroundColor = .appText
        logoutButton.translatesAutoresizingMaskIntoConstraints = false
        loginInfoStackView.addArrangedSubview(logoutButton)
        logoutButton.widthAnchor.constraint(equalTo: loginInfoStackView.widthAnchor).isActive = true
        logoutButton.heightAnchor.constraint(equalTo: wrapper.heightAnchor, multiplier: buttonHeight).isActive = true
        wrapper.addArrangedSubview(SeparatorView())
    }
    
    private func setupChangePassword() {
        wrapper.addArrangedSubview(passwordStackView)
        passwordStackView.translatesAutoresizingMaskIntoConstraints = false
        passwordStackView.widthAnchor.constraint(equalTo: wrapper.widthAnchor, multiplier: 1).isActive = true
        passwordStackView.axis = .vertical
        passwordStackView.contentMode = .scaleAspectFit
        passwordStackView.distribution = .equalSpacing
        passwordStackView.spacing = 15
        passwordStackView.alignment = .center
        
        for input in [("Old Password", oldPasswordInput), ("New Password", newPasswordInput), ("Repeat New Password", repeatPasswordInput)] {
            let textField = input.1
            let name = input.0
        
            
            let passwordInput = UIStackView()
            passwordStackView.addArrangedSubview(passwordInput)
            passwordInput.translatesAutoresizingMaskIntoConstraints = false
            passwordInput.widthAnchor.constraint(equalTo: wrapper.widthAnchor, multiplier: 1).isActive = true
            passwordInput.axis = .vertical
            passwordInput.contentMode = .scaleAspectFit
            passwordInput.distribution = .equalSpacing
            passwordInput.spacing = 5
            passwordInput.alignment = .leading
            
            let label = UILabel()
            label.text = name
            label.textColor = .appGreen
            passwordInput.addArrangedSubview(label)
            textField.placeholder = name
            textField.font = UIFont.systemFont(ofSize: 15)
            textField.borderStyle = UITextField.BorderStyle.roundedRect
            textField.autocorrectionType = UITextAutocorrectionType.no
            textField.keyboardType = UIKeyboardType.default
            textField.returnKeyType = UIReturnKeyType.done
            textField.clearButtonMode = UITextField.ViewMode.whileEditing
            textField.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
            passwordInput.addArrangedSubview(textField)
            textField.translatesAutoresizingMaskIntoConstraints = false
            textField.widthAnchor.constraint(equalTo: passwordInput.widthAnchor, multiplier: 1).isActive = true
            textField.heightAnchor.constraint(equalTo: wrapper.heightAnchor, multiplier: textInputHeight).isActive = true
            textField.textContentType = .password
            textField.isSecureTextEntry = true
        }
        
        changePasswordButton.setTitle("CHANGE PASSWORD", for: .normal)
        changePasswordButton.setTitleColor(.white, for: .normal)
        changePasswordButton.backgroundColor = .appText
        changePasswordButton.translatesAutoresizingMaskIntoConstraints = false
        passwordStackView.addArrangedSubview(changePasswordButton)
        changePasswordButton.widthAnchor.constraint(equalTo: passwordStackView.widthAnchor).isActive = true
        changePasswordButton.heightAnchor.constraint(equalTo: wrapper.heightAnchor, multiplier: buttonHeight).isActive = true
        
    }
    
    func setLoginData(label: String, provider: String) {
        emailLabel.text = label
        let image = UIImage(named: "\(provider).png")
        providerImage.image = image

        if provider == "password" {
            passwordStackView.isUserInteractionEnabled = true
            passwordStackView.alpha = 1.0
        } else {
            passwordStackView.isUserInteractionEnabled = false
            passwordStackView.alpha = 0.5
        }
    }
     
    private func setupButtonActions() {
        logoutButton.addTarget(self, action: #selector(logoutPressed), for: .touchUpInside)
        changePasswordButton.addTarget(self, action: #selector(changePasswordPressed), for: .touchUpInside)
    }
    
    @objc private func logoutPressed() {
        if let _ = delegate {
            delegate?.logoutPressed()
        }
    }
    
    @objc private func changePasswordPressed() {
        if isPasswordValid(), let _ = delegate, let old = oldPasswordInput.text, let new = newPasswordInput.text {
            delegate?.changePasswordPressed(oldPassword: old, newPassword: new)
        }
        return
    }
    
    func isPasswordValid() -> Bool {
        guard let oldPassword = oldPasswordInput.text, oldPassword.count > 0  else {
            self.oldPasswordInput.shake()
            return false
        }
        
        guard let newPassword = newPasswordInput.text, let newPasswordRepeat = repeatPasswordInput.text, newPassword == newPasswordRepeat, oldPassword.count > 0, newPassword.count > 0 else {
            self.newPasswordInput.shake()
            self.repeatPasswordInput.shake()
            return false
        }
        return true
    }
    
    func resetPasswordInputs() {
        oldPasswordInput.text = ""
        newPasswordInput.text = ""
        repeatPasswordInput.text = ""
    }
    
    required init?(coder: NSCoder) {
       super.init(coder: coder)
       self.setup()
    }

}
