//
//  SettingsView.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 13/03/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import UIKit

class SettingsView: UIView {
    
    enum Buttons: Int, CaseIterable {
        case EXPORT = 0
        case ACCOUNT = 1
        case ABOUT = 2
    }
    
    private var didSetupConstraints = false
    
    weak var delegate: SettingsVC?
    
    let wrapper = UIStackView()
    let expenseSorting = SortingOptionsStackView(labelName: "Expense sorting:", sortingValues: SettingsService.instance.getExpenseSortOptions())
    
    let incomeSorting = SortingOptionsStackView(labelName: "Income sorting:", sortingValues: SettingsService.instance.getIncomeSortOptions())
    
    let exportButton = RoundCornersUIButton()
    let accountButton = RoundCornersUIButton()
    let aboutButton = RoundCornersUIButton()
    
    let separator1 = UIView()
    let separator2 = UIView()
    
    init(){
       super.init(frame: CGRect.zero)
       self.setup()
    }
    
    private func setup() {
        self.backgroundColor = .white
        //clipsToBounds = true
        setupStackView()
        setupSortingOptions()
        setupButtons()
        setupSpaceWaster()
        self.setNeedsUpdateConstraints()
    }

    private func setupStackView() {
        self.addSubview(wrapper)
        wrapper.translatesAutoresizingMaskIntoConstraints = false
        wrapper.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.9).isActive = true
        wrapper.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.9).isActive = true
        wrapper.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        wrapper.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        wrapper.axis = .vertical
        wrapper.contentMode = .scaleAspectFit
        wrapper.distribution = .fillProportionally
        wrapper.spacing = 20
        wrapper.alignment = .center
    }
    
    private func setupSortingOptions() {
        let sortingOptionsWrapper = UIStackView()
        wrapper.addArrangedSubview(sortingOptionsWrapper)
        sortingOptionsWrapper.translatesAutoresizingMaskIntoConstraints = false
        sortingOptionsWrapper.widthAnchor.constraint(equalTo: wrapper.widthAnchor, multiplier: 1).isActive = true
        sortingOptionsWrapper.heightAnchor.constraint(equalTo: wrapper.heightAnchor, multiplier: 0.2).isActive = true
        sortingOptionsWrapper.axis = .vertical
        sortingOptionsWrapper.contentMode = .scaleAspectFit
        sortingOptionsWrapper.distribution = .equalSpacing
        sortingOptionsWrapper.spacing = 1
        sortingOptionsWrapper.alignment = .center
        
        sortingOptionsWrapper.addArrangedSubview(expenseSorting)
        expenseSorting.widthAnchor.constraint(equalTo: sortingOptionsWrapper.widthAnchor).isActive = true
        sortingOptionsWrapper.addArrangedSubview(incomeSorting)
        incomeSorting.widthAnchor.constraint(equalTo: sortingOptionsWrapper.widthAnchor).isActive = true
        
        
        //setting values
        expenseSorting.sortingValue = (SettingsService.instance.expenseSort % 10)
        expenseSorting.sortingOrder = (SettingsService.instance.expenseSort < 10 ? 0 : 1)
        incomeSorting.sortingValue = ((SettingsService.instance.incomeSort-1) % 10)
        incomeSorting.sortingOrder = (SettingsService.instance.incomeSort < 10 ? 0 : 1)
        
        expenseSorting.setSortChangedTarget(target: self, action: #selector(sortChanged(_:)))
        incomeSorting.setSortChangedTarget(target: self, action: #selector(sortChanged(_:)))
        
    }
    
    private func setupButtons() {
        let buttonsWrapper = UIStackView()
        wrapper.addArrangedSubview(buttonsWrapper)
        buttonsWrapper.translatesAutoresizingMaskIntoConstraints = false
        buttonsWrapper.widthAnchor.constraint(equalTo: wrapper.widthAnchor, multiplier: 1).isActive = true
        buttonsWrapper.heightAnchor.constraint(equalTo: wrapper.heightAnchor, multiplier: 0.50).isActive = true
        buttonsWrapper.axis = .vertical
        buttonsWrapper.contentMode = .scaleAspectFit
        buttonsWrapper.distribution = .fillProportionally
        buttonsWrapper.spacing = 5
        buttonsWrapper.alignment = .center
        
        buttonsWrapper.addArrangedSubview(SeparatorView())
        
        for b in Buttons.allCases {
            let button = RoundCornersUIButton()
            button.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
            button.setTitle(String(describing: b), for: .normal)
            button.tag = b.rawValue
            switch b {
            case Buttons.EXPORT:
                button.backgroundColor = .appGreen
                button.setTitleColor(.white, for: .normal)
            case Buttons.ACCOUNT:
                button.backgroundColor = .appText
                button.setTitleColor(.white, for: .normal)
            case Buttons.ABOUT:
                button.backgroundColor = .white
                button.setTitleColor(.appGreen, for: .normal)
            }
            button.translatesAutoresizingMaskIntoConstraints = false
            buttonsWrapper.addArrangedSubview(button)
            //button.heightAnchor.constraint(equalTo: wrapper.heightAnchor, multiplier: 0.1).isActive = true
            button.widthAnchor.constraint(equalTo: wrapper.widthAnchor, multiplier: 1).isActive = true
            if b.rawValue < Buttons.allCases.count-1 {
                buttonsWrapper.addArrangedSubview(SeparatorView())
            }
        }
    }
    
    private func setupSpaceWaster() {
//        let image = UIImage(named: "pig_logo_green.png")
//        let logo = UIImageView()
//        logo.contentMode = .scaleAspectFit
//        logo.image = image
//        wrapper.addArrangedSubview(logo)
    }
    
    
    @objc private func buttonPressed(_ sender: UIButton) {
        if let _ = delegate {
            switch sender.tag {
            case Buttons.EXPORT.rawValue:
                delegate?.exportButtonPressed()
            case Buttons.ACCOUNT.rawValue:
                delegate?.accountButtonPressed()
            case Buttons.ABOUT.rawValue:
                delegate?.aboutButtonPressed()
            default:
                return
            }
        }
    }
    
    @objc func sortChanged(_ sender: UISegmentedControl) {
        print("sort changed")
        if let _ = delegate {
            if sender == expenseSorting.sortingValueControl || sender == expenseSorting.sortingDirectionControl {
                delegate?.expenseSortingChanged(newIndex: expenseSorting.sortingValue, order: expenseSorting.sortingOrder)
            } else if sender == incomeSorting.sortingValueControl || sender == incomeSorting.sortingDirectionControl {
                delegate?.incomeSortingChanged(newIndex: incomeSorting.sortingValue, order: incomeSorting.sortingOrder)
            }
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
}
