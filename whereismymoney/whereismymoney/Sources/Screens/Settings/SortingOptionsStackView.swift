//
//  SortingOptionsStackView.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 13/03/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import UIKit

class SortingOptionsStackView: UIStackView {
    var sortingValueControl: UISegmentedControl!
    var sortingDirectionControl: UISegmentedControl!
    
    var sortingValue: Int {
        get {
            return sortingValueControl.selectedSegmentIndex
        }
        set {
            sortingValueControl.selectedSegmentIndex = newValue
        }
    }
    
    var sortingOrder: Int {
        get {
            return sortingDirectionControl.selectedSegmentIndex
        }
        set {
            sortingDirectionControl.selectedSegmentIndex = newValue
        }
    }
    
    
    init(labelName: String, sortingValues: [String]) {
        self.sortingValueControl = UISegmentedControl(items: sortingValues)
        self.sortingDirectionControl = UISegmentedControl(items: SettingsService.instance.getDirectionOptions())
        super.init(frame: CGRect.zero)
        setup(labelName: labelName, sortingValues: sortingValues)
    }
    
    required init(coder: NSCoder) {
        self.sortingValueControl = UISegmentedControl(items: [])
        self.sortingDirectionControl = UISegmentedControl(items: SettingsService.instance.getDirectionOptions())
        super.init(coder: coder)
    }
    
    private func setup(labelName: String, sortingValues: [String]) {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.axis = .vertical
        self.contentMode = .scaleAspectFit
        self.distribution = .equalSpacing
        self.spacing = 4
        self.alignment = .leading
        
        let label = UILabel()
        label.text = labelName
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        self.addArrangedSubview(label)
        label.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
        
        let slidersWrapper = UIStackView()
        self.addArrangedSubview(slidersWrapper)
        slidersWrapper.translatesAutoresizingMaskIntoConstraints = false
        slidersWrapper.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1).isActive = true
        slidersWrapper.axis = .horizontal
        slidersWrapper.contentMode = .scaleAspectFit
        slidersWrapper.distribution = .fillProportionally
        slidersWrapper.spacing = 10
        slidersWrapper.alignment = .center
            
        slidersWrapper.addArrangedSubview(sortingValueControl)
        sortingValueControl.translatesAutoresizingMaskIntoConstraints = false
        sortingValueControl.heightAnchor.constraint(equalTo: slidersWrapper.heightAnchor, multiplier: 0.4)
        sortingValueControl.translatesAutoresizingMaskIntoConstraints = false

        slidersWrapper.addArrangedSubview(sortingDirectionControl)
        sortingDirectionControl.translatesAutoresizingMaskIntoConstraints = false
        sortingDirectionControl.heightAnchor.constraint(equalTo: slidersWrapper.heightAnchor, multiplier: 0.4)
        sortingDirectionControl.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func setSortChangedTarget(target: Any?, action: Selector) {
        sortingValueControl.addTarget(target, action: action, for: .valueChanged)
        sortingDirectionControl.addTarget(target, action: action, for: .valueChanged)
    }
}
