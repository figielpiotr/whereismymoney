//
//  SettingsVC.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 14.07.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import UIKit
import Firebase

class SettingsVC: AdvertableVC, HasCustomView {
    
    typealias CustomView = SettingsView

    override func loadView() {
        view = CustomView()
        self.edgesForExtendedLayout = []
    }
    
    weak var mainVC: MainVC?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customView.delegate = self
        self.navigationItem.title = "Settings"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func expenseSortingChanged(newIndex: Int, order: Int) {
        SettingsService.instance.setExpenseSort(index: newIndex, order: order)
        if let mvc = self.mainVC {
            mvc.refreshViewsAfterSortChange()
        }
    }
    
    func incomeSortingChanged(newIndex: Int, order: Int) {
        SettingsService.instance.setIncomeSort(index: newIndex, order: order)
    }
    
    func accountButtonPressed() {
        let vc = AccountVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func exportButtonPressed() {
        let vc = ExportVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func aboutButtonPressed() {
        let vc = AboutVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }

}
