//
//  AppNavigationVC.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 13.11.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import UIKit

class AppNavigationVC: UIViewController {

    var delegate: AppNavigationDelegate?
    var saveBtn: UIBarButtonItem?
    var cancelBtn: UIBarButtonItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.hideKeyboardWhenTappedAround()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(sender:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(sender:)), name:     UIResponder.keyboardWillHideNotification, object: nil)
        
    }

    func hideSaveBtn(){
        if let btn = self.saveBtn {
            btn.isEnabled = false
            btn.tintColor = UIColor.clear
        }
    }
    
    func hideCancelBtn(){
        if let btn = self.cancelBtn {
            btn.isEnabled = false
            btn.tintColor = UIColor.clear
        }
    }
    
    func showSaveBtn(){
        guard self.saveBtn != nil else {
            self.saveBtn = UIBarButtonItem.init(title: "Save", style: .done, target: self, action: #selector(self.delegate?.save))
            showSaveBtn()
            return
        }
        if let btn = self.saveBtn {
            btn.isEnabled = true
            btn.tintColor = UIColor.appText
            self.navigationItem.rightBarButtonItem = btn
        }
    }
    
    func showCancelBtn(){
        guard self.cancelBtn != nil else {
            self.cancelBtn = UIBarButtonItem.init(title: "Done", style: .done, target: self, action: #selector(self.delegate?.hideKeyboard))
            showCancelBtn()
            return
        }
        if let btn = self.cancelBtn {
            btn.isEnabled = true
            btn.tintColor = UIColor.appText
            self.navigationItem.rightBarButtonItem = btn
        }
    }
    
    
    
    @objc func keyboardWillShow(sender: NSNotification) {
        hideSaveBtn()
        showCancelBtn()
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        hideCancelBtn()
        showSaveBtn()
    }
}
