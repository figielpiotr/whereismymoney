//
//  MainView.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 26/02/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import UIKit


class ExpenseDetailsView: UIView {

    weak var delegate: ExpenseDetailsVC?
    
    private var didSetupConstraints = false
    
    var loadSpinner: UIActivityIndicatorView!
    
    let mainStackView = UIStackView()
    let expenseDataStackView = UIStackView()
    let categoryButtons = ShakeableUIStackView(frame: .zero)
    
    var expenseName = ShakeableUiTextField()
    var expenseValue = ShakeableCurrencyField()
    var dateButton = UIButton(frame: CGRect.zero)
    var categoryLabel = UILabel(frame: .zero)

    var datePicker = UIDatePicker()
    
    let dateFormatter = DateFormatter()
    
    let wrapper = UIStackView()
    
    init(){
        super.init(frame: CGRect.zero)
        self.setup()
    }
 
    private func setup() {
        dateFormatter.dateFormat = "yyyy-MM-dd"

        self.backgroundColor = .white
        clipsToBounds = true

        setupWrapper()
        configureExpenseDataStackView()
        setupDatePicker()
        setupCategoryButtons()
        
        setupSpinner()
        self.setNeedsUpdateConstraints()
    }

    func setupWrapper() {
        self.addSubview(wrapper)
        wrapper.translatesAutoresizingMaskIntoConstraints = false
        wrapper.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.9).isActive = true
        wrapper.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.9).isActive = true
        wrapper.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        wrapper.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        wrapper.distribution = .equalSpacing
        wrapper.alignment = .center
        wrapper.axis = .vertical
        wrapper.contentMode = .scaleAspectFit
        wrapper.spacing = 5
    }
    
    private func configureExpenseDataStackView() {
        wrapper.addArrangedSubview(expenseDataStackView)
        expenseDataStackView.translatesAutoresizingMaskIntoConstraints = false
        expenseDataStackView.leftAnchor.constraint(equalTo: wrapper.leftAnchor).isActive = true
        expenseDataStackView.rightAnchor.constraint(equalTo: wrapper.rightAnchor).isActive = true
        expenseDataStackView.heightAnchor.constraint(equalTo: wrapper.heightAnchor, multiplier: 0.18).isActive = true
        expenseDataStackView.distribution = .equalSpacing
        expenseDataStackView.alignment = .center
        expenseDataStackView.axis = .vertical
        expenseDataStackView.contentMode = .scaleAspectFit
        expenseDataStackView.spacing = 0
        
        expenseDataStackView.addArrangedSubview(expenseName)

        for field in [expenseName, expenseValue] {
            field.translatesAutoresizingMaskIntoConstraints = false
            expenseDataStackView.addArrangedSubview(field)
            field.widthAnchor.constraint(equalTo: expenseDataStackView.widthAnchor).isActive = true
            field.heightAnchor.constraint(equalTo: expenseDataStackView.heightAnchor, multiplier: 0.45).isActive = true
            field.font = UIFont.systemFont(ofSize: 15)
            field.borderStyle = UITextField.BorderStyle.roundedRect
            field.autocorrectionType = UITextAutocorrectionType.no
            field.keyboardType = UIKeyboardType.default
            field.returnKeyType = UIReturnKeyType.done
            field.clearButtonMode = UITextField.ViewMode.whileEditing
            field.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        }
        expenseValue.keyboardType = .decimalPad
        expenseValue.clearButtonMode = .never
        expenseValue.returnKeyType = .done
        expenseValue.minimumFontSize = 17
        expenseValue.delegate = NumbersUITextFieldDelegate.numbersUITextFieldDelegate
    }
    
    private func setupCategoryButtons() {
        wrapper.addArrangedSubview(categoryLabel)
        categoryLabel.text = "category"
        categoryLabel.translatesAutoresizingMaskIntoConstraints = false
        categoryLabel.heightAnchor.constraint(equalTo: wrapper.heightAnchor, multiplier: 0.06).isActive = true
        categoryLabel.textAlignment = .center
        wrapper.addArrangedSubview(categoryButtons)
        categoryButtons.translatesAutoresizingMaskIntoConstraints = false
        categoryButtons.widthAnchor.constraint(equalTo: wrapper.widthAnchor, multiplier: 1).isActive = true
        categoryButtons.heightAnchor.constraint(equalTo: wrapper.heightAnchor, multiplier: 0.35).isActive = true
        categoryButtons.distribution = .equalSpacing
        categoryButtons.alignment = .center
        categoryButtons.axis = .vertical
        categoryButtons.contentMode = .center
        categoryButtons.isHidden = false
    }
    
    func configureCategoryButtons(categories: [Category], selectedCategory: Category?, action: Selector) {
        var stackView = UIStackView(frame: .zero)
        for (i, category) in categories.enumerated() {
            if i % 6 == 0 {
                stackView = UIStackView(frame: .zero)
                stackView.axis  = .horizontal
                stackView.distribution = .equalCentering
                stackView.alignment = .center
                //stackView.spacing = 20
                stackView.translatesAutoresizingMaskIntoConstraints = false
                categoryButtons.addArrangedSubview(stackView)
                //stackView.leadingAnchor.constraint(equalTo: categoryButtons.leadingAnchor).isActive = true
                //stackView.trailingAnchor.constraint(equalTo: categoryButtons.trailingAnchor).isActive = true
                //stackView.widthAnchor.constraint(equalTo: categoryButtons.widthAnchor).isActive = true
                stackView.centerXAnchor.constraint(equalTo: categoryButtons.centerXAnchor).isActive = true
                stackView.heightAnchor.constraint(equalTo: categoryButtons.heightAnchor, multiplier: 0.25).isActive = true
            }
            let button = CategoryButton(category: category)
            stackView.addArrangedSubview(button)
            button.configureView(width: categoryButtons.frame.width)
            button.widthAnchor.constraint(equalTo: wrapper.widthAnchor, multiplier: 0.14).isActive = true
            button.heightAnchor.constraint(equalTo: button.widthAnchor, multiplier: 1).isActive = true
            button.backgroundColor = UIColor.appGray
            button.layer.borderWidth = 1
            button.clipsToBounds = true
            button.addTarget(self, action: #selector(categoryButtonPressed(_:)), for: .touchUpInside)
        }
    }
    
    func setupDatePicker() {
        datePicker.datePickerMode = .date
        datePicker.translatesAutoresizingMaskIntoConstraints = false
        wrapper.addArrangedSubview(datePicker)
        datePicker.widthAnchor.constraint(equalTo: wrapper.widthAnchor).isActive = true
        datePicker.heightAnchor.constraint(equalTo: wrapper.heightAnchor, multiplier: 0.30).isActive = true
        datePicker.addTarget(self, action: #selector(self.dateChanged), for: .valueChanged)
    }

    @objc func dateChanged() {
        //self.delegate?.expenseDate = self.datePicker.date
    }
    
    @objc private func categoryButtonPressed(_ sender: CategoryButton?) {
        for case let stackView as UIStackView in self.categoryButtons.arrangedSubviews {
            for case let button as CategoryButton in stackView.arrangedSubviews {
                button.choosen = false
            }
        }
        sender?.choosen = true
        self.categoryLabel.text = sender?.category?.name
        self.delegate?.buttonSelected(sender)
    }
    
    func setCategory(category: Category) {
        categoryLabel.text = category.name
        for case let stackView as UIStackView in self.categoryButtons.arrangedSubviews {
            for case let button as CategoryButton in stackView.arrangedSubviews {
                guard let c = button.category else {
                    return
                }
                
                if c === category {
                    button.choosen = true
                } else {
                    button.choosen = false
                }
            }
        }
    }
    
    func getSelectedCategory() -> Category? {
        for case let stackView as UIStackView in self.categoryButtons.arrangedSubviews {
            for case let button as CategoryButton in stackView.arrangedSubviews {
                if button.isSelected {
                    
                }
            }
        }
        return nil
    }
    
    override func willMove(toWindow newWindow: UIWindow?) {
        super.willMove(toWindow: newWindow)
        self.setNeedsLayout()
        self.layoutIfNeeded()
        self.roundifyCategoryButtons()
    }
    
    
    private func roundifyCategoryButtons() {
        for case let stackView as UIStackView in categoryButtons.arrangedSubviews {
            stackView.spacing = (self.frame.size.width/6.6)/8
            for button in stackView.arrangedSubviews {
                button.layer.cornerRadius = button.frame.height/2
            }
        }
    }
    
    private func setupSpinner() {
        loadSpinner = UIActivityIndicatorView(style: .gray)
        self.addSubview(loadSpinner)
        loadSpinner.translatesAutoresizingMaskIntoConstraints = false
        loadSpinner.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        loadSpinner.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        loadSpinner.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.01).isActive = true
        loadSpinner.heightAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.01).isActive = true
        loadSpinner.startAnimating()
        loadSpinner.isHidden = true
    }
    
    func showSpinner() {
        self.mainStackView.isHidden = true
        self.loadSpinner.startAnimating()
        self.loadSpinner.alpha = 0
        self.loadSpinner.isHidden = false
        UIView.animate(withDuration: 0.1, animations: {
                 self.loadSpinner.alpha = 1
            }, completion:  nil)
    }
    
    func hideSpinner() {
        self.loadSpinner.alpha = 1
        UIView.animate(withDuration: 0.1, animations: {
            self.loadSpinner.alpha = 0
        }, completion:  {
           (value: Bool) in
                self.loadSpinner.isHidden = true
                self.loadSpinner.stopAnimating()
        })
        self.mainStackView.isHidden = false
    }
    
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
}
