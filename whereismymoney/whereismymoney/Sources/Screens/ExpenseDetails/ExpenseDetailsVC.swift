//
//  ExpenseDetails.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 08.04.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import UIKit
import GoogleMobileAds

struct ExpenseData {
    var category: Category
    var name: String
    var value: String
    var date: Date
}

class ExpenseDetailsVM: ScreenViewModel {
    
    // MARK: Closures
    var onNameNotValid: EmptyClosure?
    var onValueNotValid: EmptyClosure?
    var onCategoryNotValid: EmptyClosure?
    var onExpenseSaved: EmptyClosure?
    
    var expense: Expense?
    
    var currentYear: Int = Date().year()
    var currentMonth: Int = Date().month()
    lazy var defaultDate: Date = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        guard let date = dateFormatter.date(from: "\(currentYear)-\(String(format: "%02d", currentMonth))-01") else { return Date() }
        return date
    }()
    
    // MARK: Services
    var dataService: DataService?
    
    func getCategories() -> [Category] {
        return dataService?.categories ?? []
    }
    
    func getExpenseData() -> ExpenseData? {
        guard let expense = expense else { return nil }
        
        return ExpenseData(
            category: expense.category,
            name: expense.name,
            value: NumberFormatter.localizedString(from: NSNumber(value: expense.value), number: NumberFormatter.Style.currency),
            date: expense.date
        )
    }
    
    func saveExpense(category: Category?, name: String, value: String, date: Date) {
        //TODO: proper validation
        
        var valid = true
        
        if name.isEmpty {
            onNameNotValid?()
            valid = false
        }
        
        if value.doubleValue == 0.0 {
            onValueNotValid?()
            valid = false
        }
        
        guard let category = category else { onCategoryNotValid?() ; return }
        guard valid else { return }
        
        
        let now = Date()

        if let expense = expense {
            expense.name = name
            expense.value = value.doubleValue
            expense.category = category
            if(expense.date == date){
                dataService?.updateExpense(expense: expense)
            }else{
                dataService?.removeExpense(expense: expense)
                expense.date = date
                dataService?.addNewExpense(expense: expense)
            }
        } else {
            let newExpense = ExpenseFactory.ef.createExpense(date: date, name: name, value: value.doubleValue, category: category, createdAt: now)
            dataService?.addNewExpense(expense: newExpense)
            expense = newExpense
        }
        
        onExpenseSaved?()
    }
}

class ExpenseDetailsVC: AdvertableVC, AppNavigationDelegate, CategoryStackViewDelegate, UITextFieldDelegate, HasCustomView {
    
    var model: ExpenseDetailsVM?

    weak var _category: Category?
    var category: Category? {
        get {
            if let c = _category {
                return c
            }
            return nil
        }
        set {
            _category = newValue
            guard let c = newValue else {
                return
            }
            self.customView.setCategory(category: c)
        }
    }

    unowned var dataService: DataService = DataService.ds
    
    private var _expenseDate = Date()
//    var expenseDate: Date
//    {
//        get {
//            if let bp = budgetPeriod, let date = self.dateFormatter.date(from: "\(bp.year)-\(String(format: "%02d", bp.month))-01"){
//                if _expenseDate.month() != bp.month || _expenseDate.year() != bp.year {
//                    _expenseDate = date
//                }
//            }
//            return _expenseDate
//        }
//        set {
//            _expenseDate = newValue
//            self.setDate(date: _expenseDate)
//        }
//    }
    
    override func viewDidLoad() {
        //self.bannerView = self.gadBanner
        super.viewDidLoad()
        setupView()
    }
    
    func setDate(date: Date) {
        self.customView.datePicker.date = date
    }
    
    func buttonSelected(_ sender: CategoryButton?) {
        if let button = sender {
            self.category = button.category
        }
    }
    
    func hideKeyboard(){
        self.customView.expenseValue.resignFirstResponder()
        self.customView.expenseName.resignFirstResponder()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func save(){
        
        let name = customView.expenseName.text
        let value = customView.expenseValue.text
        
    
        
        
//        //TODO: proper validation
//        guard customView.expenseName.text != "", let name = customView.expenseName.text else {
//            customView.expenseName.shake()
//            if customView.expenseValue.doubleValue == 0.0 {
//                customView.expenseValue.shake()
//            }
//            if !(self.category != nil) {
//                customView.categoryButtons.shake()
//            }
//            return
//        }
//
//        let price = customView.expenseValue.doubleValue
//        guard price != 0.0 else {
//            customView.expenseValue.shake()
//            if !(self.category != nil) {
//                customView.categoryButtons.shake()
//            }
//            return
//        }
//
//        guard let category = self.category else {
//            customView.categoryButtons.shake()
//            return
//        }
//
//        customView.showSpinner()
//        let now = Date()
//
//        if (self.expense) != nil {
//            expense.name = name
//            expense.value = price
//            expense.category = category
//            if(expense.date == self.expenseDate){
//                dataService.updateExpense(expense: expense)
//            }else{
//                dataService.removeExpense(expense: expense)
//                expense.date = self.expenseDate
//                dataService.addNewExpense(expense: expense)
//            }
//        } else {
//            self.expense = ExpenseFactory.ef.createExpense(date: self.expenseDate, name: name, value: price, category: category, createdAt: now)
//            dataService.addNewExpense(expense: expense)
//        }
//        navigationController?.popViewController(animated: true)
//        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: Setup
extension ExpenseDetailsVC {
    
    func setupView() {
        guard let model = model else { return }
        
        self.customView.delegate = self
        
        customView.configureCategoryButtons(
            categories: model.getCategories(),
            selectedCategory: nil,
            action: #selector(buttonSelected(_:))
        )
        
        showSaveBtn()
        
        customView.expenseName.delegate = self
        
        if let expenseData = model.getExpenseData() {
            self.category = expenseData.category
            self.customView.expenseName.text = expenseData.name
            self.customView.expenseValue.text = expenseData.value
            self.setDate(date: expenseData.date)
            //self.expenseDate = expenseData.date
        } else {
            //self.setDate(date: self.expenseDate)
            
        }
    }
}

// MARK: CustomView
extension ExpenseDetailsVC {
    
    typealias CustomView = ExpenseDetailsView
    
    override func loadView() {
        view = CustomView()
        self.edgesForExtendedLayout = []
    }
}
