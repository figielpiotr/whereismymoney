//
//  ViewControllerFactory.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 22/01/2021.
//  Copyright © 2021 Piotr Figiel. All rights reserved.
//

import Foundation
import UIKit

protocol ViewControllerFactory {
    func makeMainVC() -> MainVC
    func makeExpenseDetailsVC(for expense: Expense) -> ExpenseDetailsVC
}

protocol FlowManagerFactory {
    func makeFlowManager() -> FlowManager
}
