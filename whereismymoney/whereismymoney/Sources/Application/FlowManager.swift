//
//  FlowManager.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 21/01/2021.
//  Copyright © 2021 Piotr Figiel. All rights reserved.
//

import Foundation
import UIKit

class ViewModelBuilder {
    
    func createMainVM(year: Int, month: Int) -> MainVM {
        return MainVM(year: year, month: month)
    }
}

protocol ScreenViewModel {}

protocol ScreenViewModelData {}

struct MainVMData: ScreenViewModelData {
    var year: Int = Date().year()
    var month: Int = Date().month()
}

class ExpenseDetailVMData: ScreenViewModelData {
    var expense: Expense?
    var currentYear: Int = Date().year()
    var currentMonth: Int = Date().month()
}

class FlowManager {
    
    enum ScreenType {
        case main
        case expenseDetail
//        case incomeList
//        case incomeDetail
//        case settings
//        case statistics
    }
        
    func navigate(from parent: UIViewController, to screenType: ScreenType, with modelData: ScreenViewModelData?) -> UIViewController? {
        var childVC: UIViewController?
        switch screenType {
            case .main: childVC = loadMainVC(modelData: modelData)
            case .expenseDetail: childVC = loadExpenseDetailVC(modelData: modelData)
//            case .incomeList: childVC = IncomeListVC()
//            case .incomeDetail: childVC = IncomeDetailsVC()
//            case .settings: childVC = SettingsVC()
//            case .statistics: childVC = StatsVC()
        }
        guard let child = childVC else { return nil }
        
        parent.navigationController?.show(child, sender: parent)
        
        return child
    }
    
    func loadMainVC(modelData: ScreenViewModelData?) -> UIViewController {
        let vc = MainVC()
        
        if let modelData = modelData as? MainVMData {
            let model = MainVM(year: modelData.year, month: modelData.month)
            model.dataService = getDataService()
            vc.model = model
        }
        
        return vc
    }
    
    func loadExpenseDetailVC(modelData: ScreenViewModelData?) -> UIViewController  {
        let vc = ExpenseDetailsVC()
        
        if let modelData = modelData as? ExpenseDetailVMData {
            let model = ExpenseDetailsVM()
            model.expense = modelData.expense
            model.currentYear = modelData.currentYear
            model.currentMonth = modelData.currentMonth
            model.dataService = getDataService()
        }

        return vc
    }
}

// MARK: Services
private extension FlowManager {
    
    func getDataService() -> DataService {
        return DataService.shared
    }
}
