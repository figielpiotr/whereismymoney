//
//  DependencyContainer.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 22/01/2021.
//  Copyright © 2021 Piotr Figiel. All rights reserved.
//

import Foundation

class DependencyContainer {
    
    private lazy var flowManager = FlowManager()
}

// MARK: - ViewControllerFactory
extension DependencyContainer: ViewControllerFactory {
    
    func makeMainVC() -> MainVC {
        return MainVC(factory: self)
    }
    
    func makeExpenseDetailsVC(for expense: Expense) -> ExpenseDetailsVC {
        return ExpenseDetailsVC()
    }
}

extension DependencyContainer: FlowManagerFactory {
    func makeFlowManager() -> FlowManager {
        return FlowManager()
    }
    
    
}
