//
//  ExportService.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 21.12.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class ExportService {
    public static var instance = ExportService()
    
    func exportAsCsv(expenses: [Expense], incomes: [Income], from: Date, to: Date) -> URL? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let fileName = "wimm_export_\(dateFormatter.string(from: from))_to_\(dateFormatter.string(from: to)).csv"
        
        let path = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName)
        var csvText = "Expenses,,,\nDate,Category,Name,Value\n"
        
        for expense in expenses {
            let newLine = "\(dateFormatter.string(from: expense.date)),\(expense.category.name),\(expense.name),\(expense.value)\n"
            csvText.append(newLine)
        }
        csvText.append("Income,,,\nDate,Name,Value,\n")
        for income in incomes {
            let newLine = "\(dateFormatter.string(from: income.date)),\(income.name),\(income.value),\n"
            csvText.append(newLine)
        }
        
        do {
            try csvText.write(to: path!, atomically: true, encoding: String.Encoding.utf8)
        } catch {
            //TODO: proper error handling
            print("Failed to create file")
            print("\(error)")
        }
        return path
    }
    
    
    func exportHTMLContentToPDF(HTMLContent: String, path: URL) {
        //let printPageRenderer = CustomPrintPageRenderer()
        let printPageRenderer = UIPrintPageRenderer()
        
        let printFormatter = UIMarkupTextPrintFormatter(markupText: HTMLContent)
        printPageRenderer.addPrintFormatter(printFormatter, startingAtPageAt: 0)
        
        let pdfData = drawPDFUsingPrintPageRenderer(printPageRenderer: printPageRenderer)
        do {
            try pdfData?.write(to: path, options: NSData.WritingOptions.atomic)
        } catch {
            //TODO: proper error handling
            print("Failed to create file")
            print("\(error)")
        }
        //pdfData?.write(toFile: path, atomically: true)
    }
    
    
    func drawPDFUsingPrintPageRenderer(printPageRenderer: UIPrintPageRenderer) -> NSData! {
        let data = NSMutableData()
        
        UIGraphicsBeginPDFContextToData(data, CGRect.zero, nil)
        print("Pages: \(printPageRenderer.numberOfPages)")
        for i in 0..<printPageRenderer.numberOfPages {
            UIGraphicsBeginPDFPage()
            printPageRenderer.drawPage(at: i, in: UIGraphicsGetPDFContextBounds())
        }
        
        UIGraphicsEndPDFContext()
        
        return data
    }
    
    func exportAsPDF(_ webView: WKWebView, _ fileName: String) -> URL? {
        let fileName = fileName
        let path = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName)
        
        
        let frmt = webView.viewPrintFormatter()
        
        //let frmt = UIMarkupTextPrintFormatter(markupText: HTMLContent)
        let render = UIPrintPageRenderer()
        render.addPrintFormatter(frmt, startingAtPageAt: 0)
        
        //  Create Paper Size for print
        
        let page = CGRect(x: 0, y: 0, width: 595.2, height: 841.8)
        let printable = page.insetBy(dx: 20, dy: 20).offsetBy(dx: 8, dy: 0)
        
        render.setValue(NSValue(cgRect: page), forKey: "paperRect")
        render.setValue(NSValue(cgRect: printable), forKey: "printableRect")
        
        // 4. Create PDF context and draw
        
        let filedata = NSMutableData()
        UIGraphicsBeginPDFContextToData(filedata, CGRect.zero, nil)
        
        print(render.numberOfPages)
        
        for i in 1...render.numberOfPages {
            
            UIGraphicsBeginPDFPage();
            let bounds = UIGraphicsGetPDFContextBounds()
            render.drawPage(at: i - 1, in: bounds)
        }
        
        UIGraphicsEndPDFContext();
        
        // 5. Save PDF file you can also save this file by using another button
        
        
        //let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        
        //filedata.write(toFile: "\(documentsPath)/myfile.pdf", atomically: true)
        filedata.write(to: path!, atomically: true)
        
        return path   
    }
    

}
