//
//  DataService.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 24.04.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase


class DataService{
    public static var ds = DataService()
    
    public static var shared: DataService {
        get {
            return self.ds
        }
    }
    
    let expenseFactory = ExpenseFactory.ef
    
    var categories = [Category]()
    var categoriesDownloaded = false
    
    var periodObserverHandler: UInt?
    
    
    let databaseRef = Database.database().reference()
    
    var budgetId: String?
    
    init() {
    }
    
    func disconnectBudgetId(){
        self.budgetId = ""
        self.stopObserving()
    }
    
    func createUserData(user: User){
        let eRef = self.databaseRef.child("budgets/").childByAutoId()
        eRef.setValue(true)
        let budgetId = eRef.key
        let userBudget = self.databaseRef.child("users/\(user.uid)/")
        userBudget.setValue(
            [
                "budgets": [budgetId: ["name": "default"]],
                "settings": SettingsService.instance.getDefaultUserSettings()
            ]
        )
    }
    
    func loadUserSettings(onComplete: @escaping FIRAuthCompletion) {
        if let user = Auth.auth().currentUser {
            let settingsPath = "users/\(user.uid)/settings"
            databaseRef.child(settingsPath).observeSingleEvent(of: .value, with: { [weak self] (snapshot) in
                if !snapshot.exists() { self?.databaseRef.child(settingsPath).setValue(SettingsService.instance.getDefaultUserSettings())
                } else if let settings = snapshot.value as? Dictionary<String, Int> {
                    for (key, value) in settings {
                        switch key {
                            case SettingsService.instance.SETTTINGS_KEY_EXPENSE_SORT:
                                SettingsService.instance.expenseSort = value
                            case SettingsService.instance.SETTTINGS_KEY_INCOME_SORT:
                                SettingsService.instance.incomeSort = value
                            default:
                                break
                        }
                    }
                }
            })
        }
        onComplete(nil, nil)
    }
    
    
    func createUserDataIfNotExist(user: User, onComplete: @escaping FIRAuthCompletion) {
        databaseRef.child("users").child(user.uid).observeSingleEvent(of: .value, with: { [weak self] (snapshot) in
            if !snapshot.exists() {
                self?.createUserData(user: user)
            }
            onComplete(nil, user)
        })
    }

    func setBudgetForUser(user: User, completionHandler:@escaping ()->()) {
        databaseRef.child("users").child(user.uid).child("budgets").observeSingleEvent(of: .value, with: { [weak self] (snapshot) in
            if let budgets = snapshot.value as? Dictionary<String, AnyObject>{
                for (bid, _) in budgets {
                    self?.budgetId = bid
                    completionHandler()
                }
            }
        })
    }
    
    func ensureYearMonthExist(budgetId: String, year: Int, month: Int){
        databaseRef.child("budgets").child(budgetId).child(formatDateForDB(year: year, month: month)).observeSingleEvent(of: .value, with: { [weak self] (snapshot) in
            if !snapshot.exists() {
                if let self = self {
                    self.budgetExist(budgetId: budgetId)
                    let x = "budgets/\(budgetId)/\(String(describing: self.formatDateForDB(year: year, month: month)))"
                    self.databaseRef.child(x).setValue(["exp": true, "inc": true])
                }
            }
        })
    }
    
    func budgetExist(budgetId: String){
        databaseRef.child("budgets").child(budgetId).observeSingleEvent(of: .value, with: { [weak self] (snapshot) in
            if !snapshot.exists() {
                self?.databaseRef.child("budgets/\(budgetId)").setValue(true)
            }
        })
    }
    
    
    private func formatDateForDB(year: Int, month: Int) -> String
    {
        let formated = "_" + String(year) + "/_" + String(format: "%02d", month)
        return formated
    }
    
    private func formatPathForDB(bid: String, type: String, year: Int, month: Int, day: Int?) -> String
    {
        var formated = "budgets/\(bid)/_"
        formated = formated + String(year) + "/_" + String(format: "%02d", month)
        if let d = day, type == "exp" {
            formated = formated + "/\(type)/_" + String(d)
        } else if type == "inc" {
            formated = formated + "/\(type)"
        }
        return formated
    }
    
    func observeBudgetPeriodForPeriod(year: Int, month: Int, completionHandler:@escaping (BudgetPeriod)->()){
        guard let budgetId = self.budgetId else {
            return
        }
        ensureYearMonthExist(budgetId: budgetId, year: year, month: month)
        stopObserving()
        
        
        self.periodObserverHandler = databaseRef.child("budgets").child(budgetId).child(formatDateForDB(year: year, month: month)).observe(DataEventType.value, with: { [weak self] (snapshot) in
            // Get user value
            var budgetPeriod = BudgetPeriod(budgetId: budgetId, year: year, month: month)
            
            if let period = snapshot.value, let bp = self?.parseBudgetPeriodData(data: period, budgetPeriod: budgetPeriod) {
                budgetPeriod = bp
            }else{
                print("Problem with connection to firebase")
            }
            completionHandler(budgetPeriod)
        })
    }
    
    func getBudgetPeriodForPeriod(year: Int, month: Int, completionHandler:@escaping (BudgetPeriod)->()){
        guard let budgetId = self.budgetId else {
            return
        }
        ensureYearMonthExist(budgetId: budgetId, year: year, month: month)
        var budgetPeriod = BudgetPeriod(budgetId: budgetId, year: year, month: month)
        
        databaseRef.child("budgets").child(budgetId).child(formatDateForDB(year: year, month: month)).observeSingleEvent(of: .value, with: {[weak self] (snapshot) in
            if let period = snapshot.value, let bp = self?.parseBudgetPeriodData(data: period, budgetPeriod: budgetPeriod) {
                budgetPeriod = bp
                completionHandler(budgetPeriod)
            }else{
                print("Problem with connection to firebase")
            }
            
        })
        
    }
    
    func getExpensesForYear(year: Int, completionHandler:@escaping ([Expense])->()){
        guard let budgetId = self.budgetId else {
            return
        }
        databaseRef.child("budgets").child(budgetId).child("_\(year)").observeSingleEvent(of: .value, with: { [weak self] (snapshot) in
            if let data = snapshot.value as? Dictionary<String, AnyObject> {
                var parsedExpenses = [Expense]()
                for (monthId, period) in data{
                    var month = monthId
                    month.removeFirst()
                    guard let m = Int(month) else{
                        print("problem with month conversion");
                        continue
                    }
                    if let periodDict = period as? Dictionary<String, AnyObject>{
                        let expenses = ExpenseParser.instance.parse(data: periodDict, year: year, month: m)
                        parsedExpenses.append(contentsOf: expenses)
                    }
                }
                completionHandler(parsedExpenses)
            }else{
                print("Problem with connection to firebase")
            }
            
        })
    }
        
    func getExpensesForPeriod(from: Date, to: Date, expenses: [Expense], incomes: [Income], completionHandler:@escaping ([Expense], [Income])->()){
        guard let budgetId = self.budgetId else {
            return
        }
        let year = from.year()
        let month = from.month()
        var currentExpenses = [Expense]()
        var currentIncome = [Income]()
        currentExpenses.append(contentsOf: expenses)
        currentIncome.append(contentsOf: incomes)
        
        
        databaseRef.child("budgets").child(budgetId).child(formatDateForDB(year: year, month: month)).observe(.value, with: { [weak self] (snapshot) in
            if let data = snapshot.value as? Dictionary<String, AnyObject> {
                let parsedExpenses = ExpenseParser.instance.parse(data: data, year: year, month: month)
                let parsedIncomes = IncomeParser.instance.parse(data: data, year: year, month: month)
                for expense in parsedExpenses {
                    if (expense.date <= to.convertToLocalTime()) {
                        currentExpenses.append(expense)
                    }
                }
                for income in parsedIncomes {
                    if (income.date <= to.convertToLocalTime()) {
                        currentIncome.append(income)
                    }
                }
            }
            if let next = from.dateByAddingMonths(monthsToAdd: 1), let nextDate = next.startOfMonth()?.convertToLocalTime() {
                if nextDate > to {
                    completionHandler(currentExpenses, currentIncome)
                } else {
                    self?.getExpensesForPeriod(from: nextDate, to: to, expenses: currentExpenses, incomes: currentIncome, completionHandler: completionHandler)
                }
            }
        }) { (error) in
            print("ni mo")
        }
    }
    
    func getExpensesAndIncomeForYear(year: Int, completionHandler:@escaping ((expenses: [Expense], income: [Income]))->()){
        guard let budgetId = self.budgetId else {
            return
        }
        databaseRef.child("budgets").child(budgetId).child("_\(year)").observeSingleEvent(of: .value, with: { [weak self] (snapshot) in
            if let data = snapshot.value as? Dictionary<String, AnyObject> {
                var parsedExpenses = [Expense]()
                var parsedIncomes = [Income]()
                for (monthId, period) in data{
                    var month = monthId
                    month.removeFirst()
                    guard let m = Int(month) else{
                        print("problem with month conversion");
                        continue
                    }
                    if let periodDict = period as? Dictionary<String, AnyObject>{
                        let expenses = ExpenseParser.instance.parse(data: periodDict, year: year, month: m)
                        parsedExpenses.append(contentsOf: expenses)
                        let incomes = IncomeParser.instance.parse(data: periodDict, year: year, month: m)
                        parsedIncomes.append(contentsOf: incomes)
                    }
                }
                completionHandler((expenses: parsedExpenses, income: parsedIncomes))
            }else{
                print("Problem with connection to firebase")
            }
            
        })
    }
        
    func getExpensesAndIncomeForCurrentQuarter(year: Int, month: Int, completionHandler:@escaping ([Expense], [Income])->()){
        guard let budgetId = self.budgetId else {
            return
        }
        databaseRef.child("budgets").child(budgetId).child("_\(year)").observeSingleEvent(of: .value, with: { [weak self] (snapshot) in
            if let data = snapshot.value as? Dictionary<String, AnyObject> {
                let quarter = Date.getQuarterFrom(month: month)
                
                var parsedExpenses = [Expense]()
                var parsedIncome = [Income]()
                for (monthId, period) in data{
                    var month = monthId
                    month.removeFirst()
                    guard let m = Int(month) else{
                        print("problem with month conversion");
                        continue
                    }
                    if(m > ((quarter-1)*3) && m < (quarter*3)+1){
                        
                        if let periodDict = period as? Dictionary<String, AnyObject>{
                            let expenses = ExpenseParser.instance.parse(data: periodDict, year: year, month: m)
                            parsedExpenses.append(contentsOf: expenses)
                            let income = IncomeParser.instance.parse(data: periodDict, year: year, month: m)
                            parsedIncome = parsedIncome + income
                        }
                    }
                }
                completionHandler(parsedExpenses, parsedIncome)
            }else{
                print("Problem with connection to firebase")
            }
            
        })
    }

    
    
    func parseBudgetPeriodData(data: Any, budgetPeriod: BudgetPeriod) -> BudgetPeriod{
        if let periodDict = data as? [String: AnyObject]
        {
            let expenses = ExpenseParser.instance.parse(data: periodDict, year: budgetPeriod.year, month: budgetPeriod.month)
            budgetPeriod.addExpenses(expenses: expenses)
            let incomes = IncomeParser.instance.parse(data: periodDict, year: budgetPeriod.year, month: budgetPeriod.month)
            budgetPeriod.addIncomes(incomes: incomes)
            
        }
        return budgetPeriod
    }
    
    
    func stopObserving(){
        if let handler = self.periodObserverHandler {
            self.databaseRef.removeObserver(withHandle: handler)
        }
    }
    
    
    
    func loadCategoriesData(completionHandler:@escaping ()->()) {
        
        DataService.ds.databaseRef.child("categories").observeSingleEvent(of: .value, with: { [weak self] (snapshot) in
            if let categoriesValues = snapshot.value as? Dictionary<String, String> {
                self?.categories = CategoriesParser.instance.parse(data: categoriesValues)
                completionHandler()
            }
        })
    }
    
    
    func categoriesHandler(){
        print("categories handler: \(self.categories)")
    }
    
    private func parseExpenseValues(expense: Expense) -> Dictionary<String, AnyObject?>{
        
        
        return [
            "name": expense.name as AnyObject,
            "val": expense.value as AnyObject,
            "cat": expense.category.id as AnyObject,
            "createdAt": expense.createdAt.timeIntervalSince1970 as AnyObject
        ]
    }
    
    func addNewExpense(expense: Expense){
        guard let budgetId = self.budgetId else {
            return
        }
        let date = expense.date
        let expenseValues = self.parseExpenseValues(expense: expense)
        let eRef = self.databaseRef.child(formatPathForDB(bid: budgetId, type: "exp", year: date.year(), month: date.month(), day: date.day())).childByAutoId()
        eRef.setValue(expenseValues)
    }
    
    func updateExpense(expense: Expense)
    {
        guard let budgetId = self.budgetId else {
            return
        }
        let date = expense.date
        let id = expense.id
        let expenseValues = self.parseExpenseValues(expense: expense)
        let eRef = self.databaseRef.child(formatPathForDB(bid: budgetId, type: "exp", year: date.year(), month: date.month(), day: date.day()) + "/\(id)")
        eRef.setValue(expenseValues)
    }
    
    func removeExpense(expense: Expense) {
        guard let budgetId = self.budgetId else {
            return
        }
        let date = expense.date
        let id = expense.id
        let eRef = self.databaseRef.child(formatPathForDB(bid: budgetId, type: "exp", year: date.year(), month: date.month(), day: date.day()) + "/\(id)")
        eRef.setValue([])
        
    }
    
    func parseIncomeValue(income: Income) -> Dictionary<String, AnyObject?>{
        return [
            "name": income.name as AnyObject,
            "val": income.value as AnyObject,
            "createdAt": income.createdAt.timeIntervalSince1970 as AnyObject
        ]
    }
    
    func addNewIncome(income: Income){
        guard let budgetId = self.budgetId else {
            return
        }
        let incomeValues = self.parseIncomeValue(income: income)
        let date = income.date
        let eRef = self.databaseRef.child(formatPathForDB(bid: budgetId, type: "inc", year: date.year(), month: date.month(), day: nil)).childByAutoId()
        eRef.setValue(incomeValues)

    }
    
    func updateIncome(income: Income){
        guard let budgetId = self.budgetId else {
            return
        }
        let date = income.date
        let id = income.id
        let incomeValues = self.parseIncomeValue(income: income)
        let eRef = self.databaseRef.child(formatPathForDB(bid: budgetId, type: "inc", year: date.year(), month: date.month(), day: nil)+"/\(id)")
        eRef.setValue(incomeValues)
    }
    
    func removeIncome(income: Income) {
        guard let budgetId = self.budgetId else {
            return
        }
        let date = income.date
        let id = income.id
        let eRef = self.databaseRef.child(formatPathForDB(bid: budgetId, type: "inc", year: date.year(), month: date.month(), day: nil)+"/\(id)")
        eRef.setValue([])
        
    }
    
    
    
    func createCategories() {
            let categories = ["Bills", "Car", "Clothes", "Cosmetics", "Eating Out", "Education", "Entertainment", "Gifts", "Health", "Holidays", "Home", "Kids", "Misc", "Pets", "Shopping", "Sports", "Transport", "Travel"]
            for category in categories {
                let eRef = self.databaseRef.child("categories").childByAutoId()
                eRef.setValue(category)
            }

    }
    
    func setExpenseDataSort(_ dataSort: Int){
        self.setSetting(key: SettingsService.instance.SETTTINGS_KEY_EXPENSE_SORT, value: dataSort)
    }
    
    func setIncomeDataSort(_ dataSort: Int){
        self.setSetting(key: SettingsService.instance.SETTTINGS_KEY_INCOME_SORT, value: dataSort)
    }
    
    func setSetting(key: String, value: Int) {
        if let user = Auth.auth().currentUser {
            let userBudget = self.databaseRef.child("users/\(user.uid)/settings/\(key)")
            userBudget.setValue(value)
        }
    }

    
}
