//
//  StatsService.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 03.07.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import Foundation

class StatsService {
    public static var ss = StatsService()
    let ds = DataService.ds
    
    public static var instance: StatsService {
        get {
            return self.ss
        }
    }
    
    
    func getStatsForBudgetPeriod(budgetPeriod: BudgetPeriod) -> Dictionary<String, Double>{
        let expenses = budgetPeriod.expenses
        let stats = getExpensesCategoryStats(expenses: expenses)
        return stats
    }
    
    func getStatsFor(budgetPeriod: BudgetPeriod, _ percent: Bool = true, _ mergeSmallVal: Bool = false) -> Dictionary<String, Double>{
        let expenses = budgetPeriod.expenses
        return getStatsFor(expenses: expenses, percent, mergeSmallVal)
    }
    
    func getStatsFor(expenses: [Expense], _ percent: Bool = true, _ mergeSmallVal: Bool = false) -> Dictionary<String, Double>{
        var result = getExpensesCategoryStats(expenses: expenses)
        if(percent) {
            result = percentify(result)
        }
        if mergeSmallVal {
            result = mergeSmallValues(result)
        }
        return result
    }
    
    func percentify(_ data: Dictionary<String, Double>) -> Dictionary<String, Double> {
        var result = Dictionary<String, Double>()
        let overall = sum(data)
        var percents = [Stats]()

        if(overall > 0 ) {
            for (name, value) in data {
                let x = Stats(name, Int(100*value/overall), ((100*value)/overall).truncatingRemainder(dividingBy: 1))
                percents.append(x)
            }
            percents = percents.sorted(by: { $0.reminder > $1.reminder })
            
            for i in 0...percents.count {
                if(sumStats(percents) < 100) {
                    percents[i].integerPart = percents[i].integerPart + 1
                } else {
                    break
                }
            }
            for s in percents {
                result[s.category] = Double(s.integerPart)
            }
        }
        
        return result
    }

    func sum(_ data: Dictionary<String, Double>) -> Double {
        var result = 0.0
        for (_, value) in data {
            result = result + value
        }
        return result
    }
    
    func mergeSmallValues(_ data: Dictionary<String, Double>) -> Dictionary<String, Double> {
        var rest = 0.0
        var dataTmp = data
        
        while(rest < 3) {
            if let tmp = dataTmp.min(by: { a, b in a.value < b.value }), tmp.value < 3 {
                rest = rest + tmp.value
                dataTmp.removeValue(forKey: tmp.key)
            } else {
                break
            }
        }
        if rest > 0 {
            dataTmp["Rest"] = rest
        }
        return dataTmp
    }
    
    private func getExpensesCategoryStats(expenses: [Expense]) -> Dictionary<String, Double>{
        var stats = self.prepareCategoryStatsArray()
        
        for expense in expenses{
            let categoryName = expense.category.name
            if let oldValue = stats[categoryName] {
                stats[categoryName] = oldValue + expense.value
            }
        }
        
        return stats
    }
    
    struct Stats {
        var category: String
        var integerPart: Int
        var reminder: Double
        
        init(_ c: String, _ i: Int, _ r : Double) {
            category = c
            integerPart = i
            reminder = r
        }
    }
    
    
    private func sumStats(_ stats: [Stats]) -> Int {
        var sum = 0
        for i in stats {
            sum = sum + i.integerPart
        }
        return sum
    }
    
    func prepareCategoryStatsArray() -> Dictionary<String, Double>{
        let categories = self.ds.categories // int:category
        var stats = Dictionary<String, Double>()
        
        for category in categories {
            stats[category.name] = 0.0
        }
        return stats
    }
    
    
    func getYearlyCategoryExpensesAccordingToBudgetPeriod(budgetPeriod: BudgetPeriod, completionHandler:@escaping (Dictionary<String, Double>, Double, Double)->()) {
        let year = budgetPeriod.year
        self.ds.getExpensesAndIncomeForYear(year: year) {
            (expenses: [Expense], income: [Income]) in
                let result = self.getStatsFor(expenses: expenses, true, true)
                let incomeSum = self.sum(income)
                let expenseSum = self.sum(expenses)
                completionHandler(result, incomeSum, expenseSum)
        }
    }
    
    func getQuarterlyCategoryExpensesAccordingToBudgetPeriod(budgetPeriod: BudgetPeriod, completionHandler:@escaping (Dictionary<String, Double>, Double, Double)->()) {
        let year = budgetPeriod.year
        let month = budgetPeriod.month
        self.ds.getExpensesAndIncomeForCurrentQuarter(year: year, month: month) {
            (expenses: [Expense], income: [Income]) in
            let expensesStats = self.getStatsFor(expenses: expenses, true, true)
            let incomeSum = self.sum(income)
            let expenseSum = self.sum(expenses)
            completionHandler(expensesStats, incomeSum, expenseSum)
        }
    }
    
    func getExpenseMonthlyStats(expenses: [Expense]) -> Dictionary<Int, Double>{
        var result = Dictionary<Int, Double>()
        for i in 1...12 {
            result[i] = 0.0
        }
        
        for expense in expenses {
            if let oldVal = result[expense.date.month()] {
                result[expense.date.month()] = oldVal + expense.value
            }
        }
        return result
    }
    
    func getIncomeMonthlyStats(incomes: [Income]) -> Dictionary<Int, Double>{
        var result = Dictionary<Int, Double>()
        for i in 1...12 {
            result[i] = 0.0
        }
        
        for income in incomes {
            if let oldVal = result[income.date.month()] {
                result[income.date.month()] = oldVal + income.value
            }
        }
        return result
    }
    
    
    
    func getYearlyIcomeExpenseAccordingToBudgetPeriod(budgetPeriod: BudgetPeriod, completionHandler:@escaping (_ expenses: Dictionary<Int, Double>, _ income: Dictionary<Int, Double>)->()) {
        let year = budgetPeriod.year
        self.ds.getExpensesAndIncomeForYear(year: year) {
            (result: (expenses: [Expense], income: [Income])) in
            let incomeStats = self.getIncomeMonthlyStats(incomes: result.income)
            let expenseStats = self.getExpenseMonthlyStats(expenses: result.expenses)
            completionHandler(expenseStats, incomeStats)
        }
    }

    
    
    func getMonthlyBudgetStatsAccordingToBudgetPeriod(){
        //TODO: expenses and incomes per monty yearly
    }
    
    func sum<T:ValueProtocol>(_ data: [T]) -> Double {
        var result: Double = 0.0
        for d in data {
            result = result + d.value
        }
        return result
    }
    
}
