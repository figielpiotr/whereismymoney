//
//  AuthService.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 14.07.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import Foundation
import FirebaseAuth
import FBSDKCoreKit
import FBSDKLoginKit
import TwitterKit
import GoogleSignIn


typealias FIRAuthCompletion = (String?, AnyObject?) -> Void

let FIR_AUTH_PROVIDER_FB = "facebook.com"
let FIR_AUTH_PROVIDER_TW = "twitter.com"
let FIR_AUTH_PROVIDER_GP = "google.com"
let FIR_AUTH_PROVIDER_EM = "password"



class AuthService {
    public static var instance = AuthService()
    var stateChangeHandler: AuthStateDidChangeListenerHandle?
    
    func createLoginHandlers()
    {

    }
    
    func removeLoginHandlers(){
    }
    
    func getLoginProvider() -> String? {
        if let providerData = Auth.auth().currentUser?.providerData.first {
            return providerData.providerID
        }
        return nil
    }
    
    func login(email: String, password: String, onComplete: @escaping FIRAuthCompletion){
        Auth.auth().signIn(withEmail: email, password: password, completion: { (data, error) in
            if error != nil {
                if let errorCode = AuthErrorCode(rawValue: (error?._code)!){
                    if(errorCode == AuthErrorCode.userNotFound){
                        Auth.auth().createUser(withEmail: email, password: password, completion:
                            { (authResult, error) in
                                if error != nil {
                                    self.processErrorCode(error: error! as NSError, onComplete: { (errorMsg, data) in
                                        onComplete(errorMsg, data)
                                    })
                                } else {
                                    if let user = authResult?.user {
                                        DataService.ds.createUserData(user: user)
                                        self.sendVerificationEmail(user: user)
                                        Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error) in
                                            if(error != nil) {
                                                self.processErrorCode(error: error! as NSError, onComplete: onComplete)
                                            } else {
                                                onComplete(nil, user?.user)
                                            }
                                        })
                                    }
                                }
                        })
                    } else {
                        self.processErrorCode(error: error! as NSError, onComplete: onComplete)
                    }
                }
            } else {
                onComplete(nil, data?.user)
            }
        })
    }
    
    func sendVerificationEmail(user: User ) {
        user.sendEmailVerification(completion: { (error) in
            
        })
    }
    
    func loginFacebook(result: LoginManagerLoginResult, onComplete: @escaping FIRAuthCompletion){
        let credential = FacebookAuthProvider.credential(withAccessToken: AccessToken.current?.tokenString ?? "")
            Auth.auth().signIn(with: credential, completion: { (data, error) in
                        if error != nil {
                            self.processErrorCode(error: error! as NSError, onComplete: onComplete)
                        } else {
                            if let user = data?.user {
                                DataService.shared.createUserDataIfNotExist(user: user, onComplete: onComplete)
                            }
                        }
                    })
        
    }
    
    func loginTwitter(session: TWTRSession?, error: Error?, onComplete: @escaping FIRAuthCompletion){
            if (session != nil) {
                let authToken = session?.authToken
                let authTokenSecret = session?.authTokenSecret
                let credential = TwitterAuthProvider.credential(withToken: authToken!, secret: authTokenSecret!)
                Auth.auth().signIn(with: credential, completion: { (data, error) in
                    if error != nil {
                        self.processErrorCode(error: error! as NSError, onComplete: onComplete)
                    } else {
                        if let user = data?.user {
                            DataService.shared.createUserDataIfNotExist(user: user, onComplete: onComplete)
                        }
                    }

                })
            }
    
    }
    
    func loginGoogle(credential: AuthCredential, onComplete: @escaping FIRAuthCompletion)
    {
        Auth.auth().signIn(with: credential, completion: { (data, error) in
            if error != nil {
                self.processErrorCode(error: error! as NSError, onComplete: onComplete)
            } else {
                if let user = data?.user {
                    DataService.shared.createUserDataIfNotExist(user: user, onComplete: onComplete)
                }
            }
        })

    }
    
    
    
    func logout(onComplete: FIRAuthCompletion) {
        var provider = ""
        if let p = self.getLoginProvider() {
            provider = p
        }
        
        do {
            try Auth.auth().signOut()
            DataService.shared.disconnectBudgetId()
            if(provider == FIR_AUTH_PROVIDER_FB)
            {
                let facebookLoginManager = LoginManager()
                facebookLoginManager.logOut()
            } else if (provider == FIR_AUTH_PROVIDER_GP) {
                GIDSignIn.sharedInstance().signOut()
            } else if (provider == FIR_AUTH_PROVIDER_TW) {
                let store = TWTRTwitter.sharedInstance().sessionStore
                if let userId = store.session()?.userID {
                    store.logOutUserID(userId)
                }
            }
        } catch let error as NSError {
            self.processErrorCode(error: error, onComplete: onComplete)
        }
        onComplete(nil, nil)
    }
    
    //TODO: naprawić
    func changePasswordForCurrentUser(_ oldPassword: String, _ newPassword: String, onComplete: @escaping FIRAuthCompletion) {
        guard let user = Auth.auth().currentUser else {
            return
        }
        
        if let email = user.email {
            let credential = EmailAuthProvider.credential(withEmail: email, password: oldPassword)
            Auth.auth().currentUser?.reauthenticate(with: credential, completion: { (data, error) in
                if error != nil {
                    self.processErrorCode(error: error! as NSError, onComplete: onComplete)
                } else {
                    Auth.auth().currentUser?.updatePassword(to: newPassword, completion: { (error) in
                        if error != nil {
                            self.processErrorCode(error: error! as NSError, onComplete: onComplete)
                        } else {
                            onComplete(nil, user)
                        }
                    })
                }
            })
        }
    }

    
    
    func processErrorCode(error: NSError, onComplete: FIRAuthCompletion) {
        if let errorCode = AuthErrorCode(rawValue: error.code){
            switch errorCode {
            case AuthErrorCode.invalidEmail:
                onComplete("Invalid email address", nil)
                break
            case AuthErrorCode.wrongPassword:
                onComplete("Wrong password", nil)
                break
            case AuthErrorCode.emailAlreadyInUse:
                onComplete("Email address already in use", nil)
                break
            case AuthErrorCode.weakPassword:
                onComplete("The password must be 6 characters long or more", nil)
                break
            default:
                onComplete("There was an error during login, please try again", nil)
                print(error)
            }
        }
    }
    
    
    
    
    
}
