//
//  SettingsService.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 16.11.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import Foundation
import Firebase

struct DataSort {
    static let ByDateASC = 0
    static let ByDateDESC = 10
    static let ByNameAsc = 1
    static let ByNameDesc = 11
    static let ByValueAsc = 2
    static let ByValueDesc = 12
}

class SettingsService {
    
    let SETTTINGS_KEY_EXPENSE_SORT = "expenseSort"
    let SETTTINGS_KEY_INCOME_SORT = "incomeSort"
    
    private var _expenseSort = 0
    private var _incomeSort = 0
    
    var expenseSort: Int {
        get {
            return _expenseSort
        }
        set {
            _expenseSort = newValue
            DataService.shared.setExpenseDataSort(_expenseSort)
        }
    }
    
    var incomeSort: Int {
        get {
            return _incomeSort
        }
        set {
            _incomeSort = newValue
            DataService.shared.setIncomeDataSort(_incomeSort)
        }
    }
    
    public static var instance = SettingsService()
    
    init() {
        
    }
    
    func setSettings(settings: Dictionary<String, AnyObject>){
        
    }
    
    func setExpenseSort(index: Int, order: Int) {
        self.expenseSort = index + order * 10
    }
    
    func setIncomeSort(index: Int, order: Int) {
        self.incomeSort = (index+1) + order*10
    }
    
    func getExpenseSort() -> Int {
            return 0
    }
    
    func getIncomeSort() -> Int {
        return 0
    }
 
    func getDefaultUserSettings() -> [String: Int] {
        return [SETTTINGS_KEY_EXPENSE_SORT: DataSort.ByDateASC, SETTTINGS_KEY_INCOME_SORT: DataSort.ByDateASC]
    }
    
    func getExpenseSortOptions() -> [String] {
        return ["Date", "Name", "Value"]
    }

    func getIncomeSortOptions() -> [String] {
        return ["Name", "Value"]
    }
    
    func getDirectionOptions() -> [String] {
        return ["Asc", "Desc"]
    }
    
    
    
}
