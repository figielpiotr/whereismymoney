//
//  FakeDataService.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 28/06/2019.
//  Copyright © 2019 Piotr Figiel. All rights reserved.
//

import Foundation

class FakeDataService {
    public static var instance = FakeDataService()
    
    private var expenseRawData = "Trouser;-KsG17o11BztIi_Eym7k;200|T-shirts;-KsG17o11BztIi_Eym7k;100|Cosmetics;-KsG17o11BztIi_Eym7l;42.35|Cosmetics;-KsG17o11BztIi_Eym7l;80.01|Cosmetics;-KsG17o11BztIi_Eym7l;17.07|Cosmetics;-KsG17o11BztIi_Eym7l;20.99|Pizza;-KsG17o11BztIi_Eym7m;39.9|Restaurant;-KsG17o11BztIi_Eym7m;80|KFC;-KsG17o11BztIi_Eym7m;15|Sandwitch;-KsG17o11BztIi_Eym7m;3.99|Chineese food;-KsG17o11BztIi_Eym7m;16|Fries;-KsG17o11BztIi_Eym7m;6.9|Ice Cream;-KsG17o11BztIi_Eym7m;30.49|Udemy Course;-KsG17o11BztIi_Eym7n;150|Finances Book;-KsG17o11BztIi_Eym7n;45|English Lessons;-KsG17o11BztIi_Eym7n;100|Movies;-KsG17o11BztIi_Eym7o;38|Zoo Tickets;-KsG17o11BztIi_Eym7o;46|Theater;-KsG17o11BztIi_Eym7o;150|Flowers;-KsG17o11BztIi_Eym7p;30|Present for son;-KsG17o11BztIi_Eym7p;86|Flowers;-KsG17o11BztIi_Eym7p;24|Flu Meds;-KsG17o2pb37becmWKaK;35.48|Doctor;-KsG17o2pb37becmWKaK;150|Pharmacy;-KsG17o2pb37becmWKaK;92.47|Blood Tests;-KsG17o2pb37becmWKaK;47|Plane Tickets;-KsG17o2pb37becmWKaL;700|Hotel;-KsG17o2pb37becmWKaL;1500|Home Depot;-KsG17o2pb37becmWKaM;43.96|Chair;-KsG17o2pb37becmWKaM;30.05|Home Depot;-KsG17o2pb37becmWKaM;103.74|Kindergarten fee;-KsG17o2pb37becmWKaN;754|Pampers;-KsG17o2pb37becmWKaN;34|Pencils;-KsG17o2pb37becmWKaN;19.9|Newspaper;-KsG17o2pb37becmWKaO;10|Magazine;-KsG17o2pb37becmWKaO;12|Fish Food;-KsG17o2pb37becmWKaP;10|Dog Food;-KsG17o2pb37becmWKaP;50|Vet;-KsG17o2pb37becmWKaP;100|Groceries Tesco;-KsG17o2pb37becmWKaQ;74.55|Bread;-KsG17o2pb37becmWKaQ;12.9|Groceries ;-KsG17o2pb37becmWKaQ;59.17|Groceries Tesco;-KsG17o2pb37becmWKaQ;107.33|Groceries;-KsG17o2pb37becmWKaQ;16.25|Groceries Lidl;-KsG17o2pb37becmWKaQ;71.02|Groceries Lidl;-KsG17o2pb37becmWKaQ;40.72|Groceries Lidl;-KsG17o2pb37becmWKaQ;100.63|Swimming Pool;-KsG17o2pb37becmWKaR;29|Gym;-KsG17o2pb37becmWKaR;20|Bus ticket;-KsG17o2pb37becmWKaS;4|Groceries Lidl;-KsG17o2pb37becmWKaQ;53.41|Groceries Tesco;-KsG17o2pb37becmWKaQ;100|Bread;-KsG17o2pb37becmWKaQ;12.9|Groceries ;-KsG17o2pb37becmWKaQ;234|Groceries Tesco;-KsG17o2pb37becmWKaQ;152.41|Groceries;-KsG17o2pb37becmWKaQ;24.04|Groceries Lidl;-KsG17o2pb37becmWKaQ;42.41|Groceries Lidl;-KsG17o2pb37becmWKaQ;24.53|Groceries Lidl;-KsG17o2pb37becmWKaQ;123.41|Cosmetics;-KsG17o11BztIi_Eym7l;42.35|Cosmetics;-KsG17o11BztIi_Eym7l;80.01|Cosmetics;-KsG17o11BztIi_Eym7l;17.07|Cosmetics;-KsG17o11BztIi_Eym7l;20.99|Movies;-KsG17o11BztIi_Eym7o;38|Museum;-KsG17o11BztIi_Eym7o;20|Theater;-KsG17o11BztIi_Eym7o;90|Pharmacy;-KsG17o2pb37becmWKaK;30|Pharmacy;-KsG17o2pb37becmWKaK;102|Pharmacy;-KsG17o2pb37becmWKaK;142.32"
    private var staticExpenseRawData = "Internet Bil;-KsG17o11BztIi_Eym7i;59|Phone Bill;-KsG17o11BztIi_Eym7i;78.52|Electricity Bill;-KsG17o11BztIi_Eym7i;141.75|Gas Bill;-KsG17o11BztIi_Eym7i;31.76|Rent;-KsG17o11BztIi_Eym7i;749.86|Fuel;-KsG17o11BztIi_Eym7j;212.19|Fuel;-KsG17o11BztIi_Eym7j;234.82"
    
    //random expenses
    private var expenseData: [Expense] = []
    //static expense each month
    private var staticExpenseData: [Expense] = []
    
    init() {
        expenseData = parseRawData(expenseRawData)
        staticExpenseData = parseRawData(staticExpenseRawData)
    }
    
    private func parseRawData(_ rawData: String) -> [Expense] {
        var result: [Expense] = []
        let expenseFactory = ExpenseFactory.instance
        let dataService = DataService.shared
        let entries = rawData.split(separator: "|")
        for entry in entries {
            let data = entry.split(separator: ";")
            let categories = dataService.categories
            if let category = categories.first(where: {$0.id == data[1]}) {
                let expense = expenseFactory.createExpense(date: Date(), name: String(data[0]), value: (Double(String(data[2])) as? Double)!, category: category, createdAt: Date())
                result.append(expense)
            }
        }
        return result
    }
    
    func getRandomExpense(date: Date) -> Expense?{
        let expense = expenseData.randomElement()
        expense?.date = date
        return expense
    }
    
    func getStaticExpenses(date: Date) -> [Expense] {
        let expenses = self.staticExpenseData
        for expense in expenses {
            expense.date = date
        }
        return expenses
    }
    
    func createFakeDataForYear(year: Int) {
        for month in 1...12 {
                createDataForMonth(year: year, month: month)
        }
    }
    
    private func createDataForMonth(year: Int, month: Int) {
        createStaticDataForMonth(year: year, month: month)
        let dataService = DataService.shared
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy.MM.dd"
        for _ in 1...Int.random(in: 20...60) {
            let day = Int.random(in: 1...30)
            
            if let date = dateFormatter.date(from: "\(year).\(month).\(day)") {
                if let expense = getRandomExpense(date: date) {
                    dataService.addNewExpense(expense: expense)
                }
            }
            usleep(useconds_t(100 * 1000))
        }
    }
    
    private func createStaticDataForMonth(year: Int, month: Int) {
        let dataService = DataService.shared
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy.MM.dd"
        if let date = dateFormatter.date(from: "\(year).\(month).\(1)") {
            let expenses = getStaticExpenses(date: date)
            for expense in expenses {
                dataService.addNewExpense(expense: expense)
            }
        }
        
    }
    
    
}
