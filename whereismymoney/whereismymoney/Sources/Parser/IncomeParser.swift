//
//  IncomeParser.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 12.08.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import Foundation


class IncomeParser {
    private static var _instance = IncomeParser()
    
    static var instance: IncomeParser {
        get {
            return self._instance
        }
    }
    
    func parse(data: Dictionary<String, AnyObject>, year: Int, month: Int) -> [Income]
    {
        var parsedIncome = [Income]()
        if data.keys.contains("inc"), let incomes = data["inc"]
        {
            if let incomesDict = incomes as? Dictionary<String, AnyObject> {
                for (incomeId, attributesDict) in incomesDict
                {
                    guard let incomeValue = attributesDict["val"] as? Double  else {print("xxx: income val conversion failed with: \(attributesDict["val"]) id: \(incomeId)");continue}
                    guard let incomeName = attributesDict["name"] as? String  else {print("xxx: name conversion failed with: \(attributesDict["name"])");continue}
                    guard let createdAt = attributesDict["createdAt"] as? Double  else {print("xxx: name conversion failed with created at: \(attributesDict["createdAt"])");continue}

                    let income = IncomeFactory.incomeFactory.createIncome(id: incomeId, year: year, month: month, name: incomeName, value: incomeValue, createdAt: Date(timeIntervalSince1970: createdAt))

                    parsedIncome.append(income)
                }
                
            }
        }
        return parsedIncome
    }


}
