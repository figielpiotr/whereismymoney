//
//  ExpenseParser.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 12.08.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import Foundation


class ExpenseParser {
    private static var _instance = ExpenseParser()
    
    static var instance: ExpenseParser {
        get {
            return self._instance
        }
    }
    
    func parse(data: Dictionary<String, AnyObject>, year: Int, month: Int) -> [Expense]{
        let categories = DataService.shared.categories
        var parsedExpenses = [Expense]()
        if data.keys.contains("exp"), let expenses = data["exp"]
        {
            if let expensesDict = expenses as? [String:AnyObject] {
                for (dayId, products) in expensesDict
                {
                    if let productsDict = products as? Dictionary<String, AnyObject>
                    {
                        for (productId, attributes) in productsDict {
                            guard let attributesDict = attributes as? Dictionary<String, AnyObject> else {print("xxx: attributes dict failed"); continue}
                            
                            var day = dayId
                            day.removeFirst()
                            guard let d = Int(day) else {print("xxx: day conversion failed");continue}
                            
                            guard let productValue = attributesDict["val"] as? Double  else {print("xxx: val conversion failed with: \(String(describing: attributesDict["val"])) id: \(productId)");continue}
                            guard let productCategoryId = attributesDict["cat"] as? String  else {print("xxx: cat conversion failed with: \(String(describing: attributesDict["cat"]))");continue}
                            guard let productName = attributesDict["name"] as? String  else {print("xxx: name conversion failed with: \(String(describing: attributesDict["name"]))");continue}
                            guard let createdAt = attributesDict["createdAt"] as? Double  else {print("xxx: name conversion failed with created at: \(String(describing: attributesDict["createdAt"]))");continue}
                            guard let index = categories.firstIndex(where: {$0.id == productCategoryId})  else {print("xxx: cat failed: \(categories)");continue}
                            let category = categories[index]
                            let e = ExpenseFactory.instance.createExpense(id: productId, year: year, month: month, day: d, name: productName, value: productValue, category: category, createdAt: Date(timeIntervalSince1970: createdAt))
                            parsedExpenses.append(e)
                        }
                    }
                }
            }

        
        }
        return parsedExpenses
    }
}
