//
//  IncomeParser.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 12.08.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import Foundation


class CategoriesParser {
    private static var _instance = CategoriesParser()
    
    static var instance: CategoriesParser {
        get {
            return self._instance
        }
    }
    
    func parse(data: Dictionary<String, String>) -> [Category]
    {
        var categories = [Category]()
        for (id, categoryName) in data{
            let c = Category(id: id, name: categoryName)
            categories.append(c)
        }
        categories.sort(by: {$0.name < $1.name})
        return categories
    }
}
