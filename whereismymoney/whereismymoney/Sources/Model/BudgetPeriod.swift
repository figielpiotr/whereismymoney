//
//  Budget.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 25.04.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import Foundation

class BudgetPeriod{
    
    var budgetId: String
    var _year: Int
    var _month: Int
    
    var _incomes: [Income] = []
    var _expenses: [Expense] = []
    
    var incomes: [Income] {
        get {
            self.sortIncomes(order: SettingsService.instance.incomeSort)
            return _incomes
        }
        set {
            _incomes = newValue
        }
    }
    
    var expenses: [Expense] {
        get
        {
            self.sortExpenses(order: SettingsService.instance.expenseSort)
            return _expenses
        }
    }
    
    func sortExpenses(order: Int) {
        switch order {
        case DataSort.ByDateASC:
            _expenses.sort(by: {$0.date < $1.date || ($0.date == $1.date && $0.createdAt < $1.createdAt)})
        case DataSort.ByDateDESC:
            _expenses.sort(by: {$0.date > $1.date || ($0.date == $1.date && $0.createdAt > $1.createdAt)})
        case DataSort.ByNameAsc:
            _expenses.sort(by: {$0.name < $1.name || ($0.name == $1.name && $0.createdAt < $1.createdAt)})
        case DataSort.ByNameDesc:
            _expenses.sort(by: {$0.name > $1.name || ($0.name == $1.name && $0.createdAt > $1.createdAt)})
        case DataSort.ByValueAsc:
            _expenses.sort(by: {$0.value < $1.value || ($0.value == $1.value && $0.createdAt < $1.createdAt)})
        case DataSort.ByValueDesc:
            _expenses.sort(by: {$0.value > $1.value || ($0.value == $1.value && $0.createdAt > $1.createdAt)})
        default:
            _expenses.sort(by: {$0.date < $1.date || ($0.date == $1.date && $0.createdAt < $1.createdAt)})
        }
    }
    
    func sortIncomes(order: Int) {
        switch order {
        case DataSort.ByDateASC:
            _incomes.sort(by: {$0.date < $1.date || ($0.date == $1.date && $0.createdAt < $1.createdAt)})
        case DataSort.ByDateDESC:
            _incomes.sort(by: {$0.date > $1.date || ($0.date == $1.date && $0.createdAt > $1.createdAt)})
        case DataSort.ByNameDesc:
            _incomes.sort(by: {$0.name < $1.name || ($0.name == $1.name && $0.createdAt < $1.createdAt)})
        case DataSort.ByNameAsc:
            _incomes.sort(by: {$0.name > $1.name || ($0.name == $1.name && $0.createdAt > $1.createdAt)})
        case DataSort.ByValueAsc:
            _incomes.sort(by: {$0.value < $1.value || ($0.value == $1.value && $0.createdAt < $1.createdAt)})
        case DataSort.ByValueDesc:
            _incomes.sort(by: {$0.value > $1.value || ($0.value == $1.value && $0.createdAt > $1.createdAt)})
        default:
            _incomes.sort(by: {$0.date < $1.date || ($0.date == $1.date && $0.createdAt < $1.createdAt)})
        }
    }
    
    
    
    
    func removeExpense(expense: Expense)
    {
        if let i = _expenses.firstIndex(where: {$0.id == expense.id})
        {
            _expenses.remove(at: i)
        }
    }
    
    func removeIncome(income: Income)
    {
        if let i = incomes.firstIndex(where: {$0.id == income.id})
        {
            incomes.remove(at: i)
        }
    }
    
    var year: Int {
        get{
            return _year
        }
        set{
            _year = newValue
        }
    }
    
    var month: Int {
        get{
            return _month
        }
        set{
            _month = newValue
        }
    }
    
    
    
    init(budgetId: String, year: Int, month: Int) {
        self.budgetId = budgetId
        self._year = year
        self._month = month
    }
    
    func addIncome(income: Income)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy.MM.dd"
        if let date = dateFormatter.date(from: "\(self._year).\(self._month).01") {
            income.date = date
            self.incomes.append(income)
        }
    }
    
    func addIncomes(incomes: [Income]){
        self.incomes = self.incomes + incomes
    }
    
    func addExpense(expense: Expense)
    {
        self._expenses.append(expense)
    }
    
    func addExpenses(expenses: [Expense])
    {
        self._expenses = self._expenses + expenses
    }
    
    func calculateIncome() -> Double{
        var calculatedIncome = 0.0
        for income in incomes{
            calculatedIncome += income.value
        }
        return calculatedIncome
    }
    
    func calculateExpenses() -> Double{
        var calculatedExpenses = 0.0
        for expense in _expenses{
            calculatedExpenses += expense.value
        }
        return calculatedExpenses
    }
    
    func countExpenses() -> Int {
        return self._expenses.count
    }
    
    func getExpensesStats() -> Dictionary<Int, Double>{
        let result = Dictionary<Int, Double>()
//        for expense in _expenses{
//            let categoryId = expense.category.id
//            if result.index(forKey: categoryId) == nil {
//                result[categoryId] = expense.value
//            }else{
//                if let val = result[categoryId] {
//                    result[categoryId] = val + expense.value
//                }
//            }
//        }
        return result
    }
    
    func clearData() {
        self._expenses.removeAll()
        self._incomes.removeAll()
    }
    
    
}
