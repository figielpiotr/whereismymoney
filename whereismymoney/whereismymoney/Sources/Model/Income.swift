//
//  Income.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 25.04.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import Foundation

class Income: ValueProtocol {
    var id: String
    private var _name: String
    private var _value: Double
    private var _date: Date
    private var _createdAt: Date

    var name: String {
        get {
            return _name
        }
        set {
            _name = newValue
        }
    }
    
    var value: Double {
        get {
            return _value
        }
        set {
            _value = newValue.roundTo(places: 2)
        }
    }
    
    var createdAt: Date {
        get {
            return _createdAt
        }
        set {
            _createdAt = newValue
        }
    }
    
    var date: Date {
        get {
            return _date
        }
        set {
            _date = newValue
        }
    }
    
    init(id: String, name: String, value: Double, date: Date, createdAt: Date) {
        self.id = id
        _name = name
        _value = value.roundTo(places: 2)
        _createdAt = createdAt
        _date = date
    }
}
