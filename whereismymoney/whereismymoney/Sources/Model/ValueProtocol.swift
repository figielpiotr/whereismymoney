//
//  Value.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 19.02.2018.
//  Copyright © 2018 Piotr Figiel. All rights reserved.
//

import Foundation

protocol ValueProtocol {
    var value: Double { get set }

}
