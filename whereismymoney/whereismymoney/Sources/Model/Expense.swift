//
//  Expense.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 25.04.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import Foundation

class Expense: ValueProtocol {
    var id: String
    var date: Date
    var name: String
    var value: Double
    var category: Category
    var createdAt: Date
    
    init(date: Date, name: String, value: Double, category: Category, createdAt: Date) {
        self.id = ""
        self.date = date
        self.name = name
        self.value = value.roundTo(places: 2)
        self.category = category
        self.createdAt = createdAt
    }
    
    init(id: String, date: Date, name: String, value: Double, category: Category, createdAt: Date) {
        self.date = date
        self.name = name
        self.value = value.roundTo(places: 2)
        self.category = category
        self.id = id
        self.createdAt = createdAt
    }
    
    func getDay() -> Int {
        return date.day()
    }
}
