//
//  Advertable.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 01.03.2018.
//  Copyright © 2018 Piotr Figiel. All rights reserved.
//

import Foundation
import UIKit
import GoogleMobileAds



protocol Advertable {
    var bannerView: GADNativeExpressAdView! { set get }
    //func prepareAds(gadBanner: GADNativeExpressAdView!)
}

extension Advertable where Self: UIViewController {
    func prepareAds() {
//        bannerView.adUnitID = GAD_UNIT_ID
//        bannerView.rootViewController = self
//        let request = GADRequest()
//        bannerView.load(request)
    }
}

