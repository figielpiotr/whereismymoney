//
//  CategoryStackViewDelegate.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 24.11.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import Foundation

@objc
protocol CategoryStackViewDelegate {
    @objc func buttonSelected(_ sender: CategoryButton?)

}
