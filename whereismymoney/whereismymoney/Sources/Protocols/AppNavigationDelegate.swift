//
//  AppNavigationDelegate.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 13.11.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import Foundation

@objc
protocol AppNavigationDelegate {
    @objc func save()
    @objc func hideKeyboard()
}
