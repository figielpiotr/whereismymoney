//
//  HTMLReportFactory.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 23.01.2018.
//  Copyright © 2018 Piotr Figiel. All rights reserved.
//

import Foundation


class HTMLReportFactory {
    
    static var instance = HTMLReportFactory()
    let dateFormatter = DateFormatter()

    init() {
        dateFormatter.dateFormat = "yyyy-MM-dd"
    }
    
    private func getDummyExpense() -> Expense{
        let categories = DataService.shared.categories
        let category = categories[Int(arc4random_uniform(UInt32(categories.count)))]
        let expense = ExpenseFactory.instance.createExpense(date: Date(), name: "test", value: 123.32, category: category, createdAt: Date())
        return expense
    }
    
    private func getDummyIncome() -> Income {
        return Income(id: "adfasdfasdf", name: "income", value: 42.23, date: Date(), createdAt: Date())
    }
    
    private func getContentsOf(resource: String, ofType: String) -> String? {
        guard let file = Bundle.main.path(forResource: resource, ofType: ofType) else {
            return nil
        }
        do {
            return try String(contentsOfFile: file)
        }
        catch {
            print("\(error)")
            print("Unable to open and use HTML template files.")
        }
        return nil
    }
    func getHTMLHeader(_ expenses: [Expense], _ income: [Income], _ from: Date, _ to: Date) -> String {
        guard var contents = getContentsOf(resource: "PDFExportTemplate_Header", ofType: "html") else {
            return "<!DOCTYPE html><html><head>"
        }
        if let chartJS = getContentsOf(resource: "Chart.min", ofType: "js") {
            contents = contents.replacingOccurrences(of: "%CHART_JS%", with: chartJS)
        }
        if let logo = getContentsOf(resource: "pdf_report_logo", ofType: "txt") {
            contents = contents.replacingOccurrences(of: "%PNG_LOGO%", with: logo)
        }
        contents = contents.replacingOccurrences(of: "%DATE_FROM%", with: dateFormatter.string(from: from))
        contents = contents.replacingOccurrences(of: "%DATE_TO%", with: dateFormatter.string(from: to))
        let expensesValue = StatsService.instance.sum(expenses)
        let incomeValue = StatsService.instance.sum(income)
        contents = contents.replacingOccurrences(of: "%OVERALL_INCOME%", with: NumberFormatter.localizedString(from: NSNumber.init(value: incomeValue), number: NumberFormatter.Style.currency))
        contents = contents.replacingOccurrences(of: "%OVERALL_EXPENSE%", with: NumberFormatter.localizedString(from: NSNumber.init(value: expensesValue), number: NumberFormatter.Style.currency))
        
        let stats = StatsService.instance.getStatsFor(expenses: expenses, false)
        let percentStats = StatsService.instance.getStatsFor(expenses: expenses)
        
        let chartDataString = Array(stats.values).map{ String($0)}.joined(separator: ", ")
        
        var tmp: [String] = []
        for (categoryName, value) in percentStats {
            tmp.append("\"\(value)% : \(categoryName)\"")
        }
        let chartLegendString = tmp.joined(separator: ", ")
        
        
        contents = contents.replacingOccurrences(of: "%CHART_DATA%", with: chartDataString)
        contents = contents.replacingOccurrences(of: "%CHART_LEGEND%", with: chartLegendString)
        
        
        
        return contents
    }
    
    func getHTMLFooter() -> String {
        return "</body></html>"
    }
    
    func getIncomeTableHeader() -> String {
        return "<table id=\"incomes\" class=\"flex-item\">\n<tr>\n<th>Date</th>\n<th>Name</th>\n<th>Value</th>\n</tr>"
    }
    
    func getExpenseTableHeader() -> String {
        return "<table id=\"expenses\" class=\"flex-item\">\n<tr>\n<th>Date</th>\n<th>Category</th>\n<th>Name</th>\n<th>Value</th>\n</tr>"
    }
    
    func create(expenses: [Expense], incomes: [Income], from: Date, to: Date) -> String {
        
        let maxRowsOnFirstPage = 10
        let maxRowsPerPage = 40

        var HTMLContent = getHTMLHeader(expenses, incomes, from, to)
        

        var expenseTablesStrings: [String] = []
        var incomeTablesStrings: [String] = []
        var table = ""
      
        if incomes.count > 0 {
            HTMLContent = HTMLContent + "<h2>Income</h2>"
            var divider = maxRowsOnFirstPage
            var tablesCounter = 0
            var k = 0
            table = getIncomeTableHeader()
            for (index, income) in incomes.enumerated() {
                table = table + "<tr><td>\(dateFormatter.string(from: income.date))</td><td>\(income.name)</td><td>\(income.value)</td></tr>\n"

                if (((index+1+k)%divider == 0 && index != 0 && index != maxRowsPerPage-1) || index == incomes.count-1) {
                    table = table + "</table>"
                    incomeTablesStrings.append(table)
                    tablesCounter = tablesCounter + 1
                    if tablesCounter >= 3 {
                        divider = maxRowsPerPage
                        k = 10
                    }
                    table = getIncomeTableHeader()
                }
            }
        }
        

        
        for (index, table) in incomeTablesStrings.enumerated() {
            if index%3 == 0 && index < maxRowsOnFirstPage*3 {
                HTMLContent = HTMLContent + "<div id=\"flex-container\">"
            } else if index%3 == 0 && index > maxRowsOnFirstPage*3 {
                HTMLContent = HTMLContent + "<div id=\"flex-container\" class=\"pagebreak-before\">"
            }
            HTMLContent = HTMLContent + table

            if index%3 == 2 || index+1 == incomeTablesStrings.count {
                HTMLContent = HTMLContent + "</div>"
            }
        }
        
        table = ""
        
        if expenses.count > 0 {
            HTMLContent = HTMLContent + "<h2 class=\"pagebreak-before\">Expenses</h2>"
            table = getExpenseTableHeader()
            for (index, expense) in expenses.enumerated() {
                table = table + "<tr><td>\(dateFormatter.string(from: expense.date))</td><td>\(expense.name)</td><td>\(expense.category.name)</td><td>\(expense.value)</td></tr>\n"
                if (((index+1)%maxRowsPerPage == 0 && index != 0) || index == expenses.count-1) {
                    table = table + "</table>"
                    expenseTablesStrings.append(table)
                    table = getExpenseTableHeader()
                }
            }
        }
        
        for (index, table) in expenseTablesStrings.enumerated() {
            if index%3 == 0 && index < 3 {
                HTMLContent = HTMLContent + "<div id=\"flex-container\">"
            } else if index%3 == 0 && index >= 3 {
                HTMLContent = HTMLContent + "<div id=\"flex-container\" class=\"pagebreak-before\">"
            }
            HTMLContent = HTMLContent + table
            
            if index%3 == 2 || index+1 == expenseTablesStrings.count {
                HTMLContent = HTMLContent + "</div>"
            }
        }
        

        
        HTMLContent = HTMLContent + getHTMLFooter()
        
    
        return HTMLContent
    }
    
    
    
    
    
    
}
