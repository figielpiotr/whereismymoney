//
//  ExpenseFactory.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 25.04.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import Foundation

class ExpenseFactory{
    static var ef = ExpenseFactory()
    
    public static var instance: ExpenseFactory {
        get {
            return self.ef
        }
    }
    
    func createExpense(id: String, year: Int, month: Int, day: Int, name: String, value: Double, category: Category, createdAt: Date) -> Expense{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy.MM.dd"
        let date = dateFormatter.date(from: "\(year).\(month).\(day)")
        let expense = Expense(id: id, date: date!, name: name, value: value, category: category, createdAt: createdAt)
        return expense
    }
    
    func createExpense(date: Date, name: String, value: Double, category: Category, createdAt: Date) -> Expense{
        let expense = Expense(date: date, name: name, value: value, category: category, createdAt: createdAt)
        return expense
    }
    
}
