//
//  IncomeFactory.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 19.06.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import Foundation

class IncomeFactory {
    static var incomeFactory = IncomeFactory()
    
    func createIncome(id: String, name: String, value: Double, createdAt: Date) -> Income{
        let income = Income(id: id, name: name, value: value, date: Date(), createdAt: createdAt)
        return income
    }
    
    
    func createIncome(id: String, year: Int, month: Int, name: String, value: Double, createdAt: Date) -> Income{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy.MM.dd"
        let date = dateFormatter.date(from: "\(year).\(month).01")
        
        let income = Income(id: id, name: name, value: value, date: date!, createdAt: createdAt)
        return income
    }
    
}
