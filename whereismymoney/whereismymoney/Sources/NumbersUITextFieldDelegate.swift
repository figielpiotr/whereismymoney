//
//  NumbersUITextViewDelegate.swift
//  whereismymoney
//
//  Created by Piotr Figiel on 21.06.2017.
//  Copyright © 2017 Piotr Figiel. All rights reserved.
//

import Foundation
import UIKit

class NumbersUITextFieldDelegate: NSObject, UITextFieldDelegate {
    public static var numbersUITextFieldDelegate = NumbersUITextFieldDelegate()
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        let countdots =  (textField.text?.components(separatedBy: (".")).count)! - 1
//        let countcommas =  (textField.text?.components(separatedBy: (",")).count)! - 1
//        if (countdots > 0  && string == ".") && (countcommas > 0 && string == ",") {
//            return false
//        }
        
        let invalidCharacters = CharacterSet(charactersIn: "0123456789.,").inverted
        return string.rangeOfCharacter(from: invalidCharacters, options: [], range: string.startIndex ..< string.endIndex) == nil
    }
}
